all:
	pdflatex phd.tex
	evince phd.pdf &

bib:
	pdflatex phd.tex
	bibtex phd.aux
	pdflatex phd.tex

idx:
	touch phd.nls
	rm phd.nls
	pdflatex phd.tex
	makeindex phd.nlo -s /usr/share/texmf-texlive/makeindex/nomencl/nomencl.ist -o phd.nls
	makeindex phd.nlo -s /usr/share/texmf-texlive/makeindex/nomencl/nomencl.ist -o phd.nls
	pdflatex phd.tex

full:
	touch phd.nls
	rm phd.nls
	pdflatex phd.tex
	bibtex phd.aux
	makeindex phd.nlo -s /usr/share/texmf-texlive/makeindex/nomencl/nomencl.ist -o phd.nls
	makeindex phd.nlo -s /usr/share/texmf-texlive/makeindex/nomencl/nomencl.ist -o phd.nls
	pdflatex phd.tex
	pdflatex phd.tex
	evince phd.pdf &
	
clean:
	rm phd.pdf phd.bbl phd.blg phd.lof phd.lot phd.out phd.toc *.aux *.log
