%!TEX root = phd.tex


\chapter{3D Modeling}



\section{Meshes and articulated models}


\subsection{3D triangle meshes}


\subsection{Articulated models}

Let $t \in \mathbb{R}^3$ a translation vector, $R \in \mathbb{R}^{ 3\times 3}$ a rotation matrix and $(s_x, s_y, s_z) \in \mathbb{R}_{+}^3$ a vector of three scale factors.
With the scale matrix $S$ defined as
\begin{equation}
	S = \begin{pmatrix}
		s_x & 0 & 0 \\
		0 & s_y & 0 \\
		0 & 0 & s_z
	\end{pmatrix}
\end{equation}
one defines the transformation matrix $T \in \mathbb{R}^{4\times 4}$ as
\begin{equation}
	T(t,R,S) = \begin{pmatrix}
		R \, S & t \\
		0 & 1
	\end{pmatrix} \,.
\end{equation}

A {\bf armature bone} $b=(t, R, s, l)$ is defined by $t$, $R$ and $s$ as defined above and the bone length $l \in \mathbb{R}_{+}$.
Using this data the bone head is defined as
\begin{equation}
	head(b) := t
\end{equation}
and its tail as
\begin{equation}
	tail(b) := t + R \,\begin{pmatrix}0\\s_y\\0\end{pmatrix} \,.
\end{equation}
This follows the convention that the bone direction is given by the y-axis.

\fbox{illustration of a bone with transformation matrix}

An {\bf armature} is a directed tree $A = (B, E)$, where $B=(b_i)_{1 \le i \le N}$ is a list of bones and $E$ a list of directed edges.
Using the tree structure each bone $b_i$ has a local transformation matrix $T_i = T(b_i)$ and a global transformation matrix $T_i^w$ computed as
\begin{equation}
	T_i^w = T_j^w \, T_i
\end{equation}
where $j$ is the index of the parent bone of bone $i$.
For the root bone, the global transformation matrix is equal to the local transformation matrix.

\fbox{illustration of a model with three bones (e.g. 3 bones examples in golem)}

Note, that the head of a bone is not necessarily equal to the tail of the parent bone.
Bone head and tail are mainly used for illustrative purposes. The armature structure is defined by the bone transformation matrices.

In general each bone has an associated {\bf pose} $p=(t_{\delta},R_{\delta},s_{\delta})$ where $t_{\delta}$, $R_{\delta}$ and $s_{\delta}$ describe a change in bone head position, bone orientation and in bone scale factors respectively.
When ''posing'' a bone, its local transformation matrix changes accordingly
\begin{equation}
	T(b,p) = T(t + t_{\delta}, R_{\delta} R, s * s_{\delta}) \,.
\end{equation}

This gives the concept of an {\bf articulated model} $M = (A, P)$, where $A$ is an armature and $P=(p_i)_{1 \le i \le N}$ a list of poses, one for each bone.
Global bone transformation matrices $T_i^w$ for an articulated model are computed like for an armature but by using positioned local transformation matrices
\begin{equation}
	T_i^w = T_j^w \, T(b_i,p_i)
\end{equation}


\subsection{Parameterizing the bone pose}

For optimizing the bone pose a thorough consideration of the pose space is required as position, rotation and scale factors require different mathematical objects.

\subsubsection{The translation space $\mathbb{R}^n$}

Translations of objects in space are generally represented by $\mathbb{R}$, a three dimensional vector space over $\mathbb{R}$ with an orthogonal basis vectors, also called a Cartesian coordinate system.

In general, a vector space over a field $k$ (called the scalars) is a set $V$ (called the vectors) together with two operations, vector addition and scalar multiplications, which satisfy a list of axioms.
Namely $V$ is an abelian group with respect to vector addition as the group operation and scalar multiplication is compatible with vector addition.

\subsubsection{The 3D rotation group $SO(3)$}

The special orthogonal group $SO(3)$ is a much more involved mathematical object.

A common representation of $SO(3)$ are quaternions $\mathbb{H}$.
As a set $\mathbb{H}$ is equal to $\mathbb{R}^4$, a four dimensional vector space over the real numbers.
Addition and scalar multiplications of quaternions are given through the vector space.
The multiplicative group structure is defined by the quaternion multiplication, also called the Hamilton product.

\fbox{Conversion $\mathbb{H} \leftrightarrow SO(3)$}

The normalization constraint yields points on a four dimensional sphere.

\subsubsection{The 2D rotation group $SO(2)$}

The special orthogonal group $SO(2)$ is a like $SO(3)$ ...

\subsubsection{The scale space}

Scale factors are represented by positive, non-zero reals ...


\subsection{Constraining the pose space}

The pose of a bone is characterized by nine parameters: three for the head translation, three for the bone orientation and three for the scale factors.
For the purposes of human motion tracking, head position and scale factors are normally not changing as the structure of a articulated human model is defined by its skeleton which is generally fixed.
An excemption is the position of the root bone which is in general unconstraint as it describes the overall position of the whole model.

Most interesting is are the change in bone rotation as these define the configuration of human joints and the pose of limbs.
The anatomy of human joints is complex as a lot of joints consists of multiple bones and a high number of muscles controling and constraining the joint movement.
In the following several basic joint types are described which can be used to approximate real human joints.

In many applications where bones represent physical objects and joints link two such bones together, it is physically not possible two realize arbitrary joint configurations.
In the human body joints are often configured to operate well in certain poses but do only allow a specific amount of twist or inclination without breaking.

In the following some basic and commonly used joints are presented.

\begin{table}[h!]
	\centering
	\begin{tabular}{
		l
		|
		>{\centering\arraybackslash}m{0.2\textwidth}
		>{\centering\arraybackslash}m{0.2\textwidth}
		>{\centering\arraybackslash}m{0.2\textwidth}
	}
	\toprule
	Type
	& Translation & 2D rotation & 3D rotation
	\\ \midrule
	Representation
	& $\mathbb{R}^3$ & $SO(2)$ & $SO(3)$
	\\
	Dimension
	& 3 & 1 & 3
	\\
	Parameterization
	& $\mathbb{R}^3$ & $\mathbb{R}$ & $\mathbb{H}$
	\\ \midrule
	Joints & - & Hinge &
		Ball \& Socket, Condyloid, 
	\\ \midrule
	Contraints
	& Box & Interval & various
	\\ \midrule
	Examples in human anatomy 
	& General position & Knee, Fingers & Hip, Shoulder, Thumb
	\\ \bottomrule
	\end{tabular}
	\caption{Bone pose and joint types}
	\label{tab:joints}
\end{table}

\subsubsection{Free an fixed joints}

A fixed joint is simply a joint where no movement is allowed and the pose is always the identity pose.
\begin{equation}
	P_{fix} = (\bm 0, I, \bm 1)
\end{equation}

A free joint in contrast is a joint which allows every possible movement.
Scale factors are still assumed to be 1.
The root node is in general a free joint.
\begin{equation}
	P_{free}(\bm x, q) = (\bm x, R(q), \bm 1)
\end{equation}
The space of quaternions $\mathbb{H}$ is chosen for the parameterization of rotations.
$R : \mathbb{H} \rightarrow \mathbb{R}^{3\times 3}$ gives the rotation representing a given quaterion $q \in \mathbb{H}$.

\subsubsection{The hinge joint}

A hinge joint allows only rotation around a fixed axis and has one parameter $\alpha \in SO(2)$.
\begin{equation}
	P_{hinge}(\alpha \,|\, a) = (\bm 0, R_a(\alpha), \bm 1)
\end{equation}
$R_a : SO(2) \rightarrow \mathbb{R}^{3\times 3}$ gives the rotation matrix defined by rotation around the axis $a \in \mathbb{R}^3$.

As hinge joints only have one degree of freedom, the rotation $\alpha$ around the fixed axis can be easily represented by a real number.
An easy possibilty for constraining the hinge joint rotation is constraining the angle to an interval $\alpha \in [\alpha_{min}, \alpha_{max}] \subset \mathbb{R}$.

In the human body the knee joint and the interphalangeal finger joints are well approximated by hinge joints.

\fbox{Illustration of a hinge joint}

\subsubsection{The ball and socket joint}

A ball and socket joint allows an arbitrary rotation.
\begin{equation}
	P_{ball}(q) = (\bm 0, R(q), \bm 1)
\end{equation}

\fbox{Illustration of a ball and socket joint}

The ball and socket joint is considered to be unconstraint and as such does not appear in the human body.

\subsubsection{The cone twist joint}

Constraints on the ball and socket joint are more difficult to describe as $SO(3)$ can not be parameterized explicitly.
The cone twist joint is one possibility to achive this.

In the human body the hip joint is a cone twist joint.
The shoulder has similarities to one and can be modeled by one, it is however a very complex joint \cite{}.


\section{Physics Simulation}


\subsection{Physical properties of articulated models}

\begin{itemize}
	\item joint constraints
	\item collision bounds
	\item mass, inertia tensor
\end{itemize}


\subsection{The dynamics equations and solving}




\section{The Golem Library and Blender}




