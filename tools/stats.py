import subprocess
import os
import datetime

def file_find():
#	os.chdir("/mydir")
	return [x for x in os.listdir(".") if x.endswith(".tex")]

# def file_len(fname):
#     with open(fname) as f:
#         for i, l in enumerate(f):
#             pass
#     return i + 1

# def create_data_dict():
# 	return {x : file_len(x) for x in file_find()}

# def data_dict_str(data):
# 	result = ''
# 	for x,n in data.iteritems():
# 		result = result + '{"' + x + '",' + str(n) + '}, '
# 	return '{' + result[:-2] + '}'

# def write(now, data):
# 	now_str = now.strftime("%Y%m%d_%H%M%S.txt")
# 	with open(now_str,'w') as f:
# 		f.write(data_dict_str(data))

#now = datetime.datetime.now()
#print now

#data = create_data_dict()
#print data

#write(now, data)

files = file_find()
files.append("morgue.tex")
files.append("daspseg.tex")
files.append("dasv.tex")
files.append("edvs.tex")
files.append("golem.tex")
files.append("hakara.tex")
files.append("q0.tex")
files.append("romeo.tex")
print files

for x in files:
	print '>>> Procssing', x
	#ruby file-size.rb phd.tex | stats/phd.tex.log
	with open('stats/'+x+'.log','w') as f:
		subprocess.call(['ruby','tools/file-size.rb',x], stdout=f)
