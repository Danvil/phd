import sys
import operator
import string
from collections import Counter

def candidateWord(files):
	text = ''
	for fn in files:
		with open(fn, 'r') as f:
			text = text + f.read()

	text = string.lower(text)
	for ch in """!"#$%&()*+^,-./:;<=>?@[\\]?_'`{|}?""":
		text = string.replace(text, ch,' ')
	words = string.split(text)
	counter = Counter(words)

	# print("\n".join("{} {}".format(*p) for p in counter.most_common()))
	max_freq = counter.most_common()[0][1]
	for word, freq in sorted(counter.most_common(), key=lambda p: (-p[1], p[0])):
		number_of_asterisks = (50 * freq ) // max_freq     # (50 * N) / M
		asterisks = '*' * number_of_asterisks        # the (50*N)/M asterisks
		print('{:>50} {}'.format(asterisks, word))

files = ['../intro.tex', '../asp.tex', '../dasp.tex', '../sdasp.tex', '../tdasp.tex']
candidateWord(files)
