%!TEX root = phd.tex


\chapter{Event-based 3D SLAM}
\label{sec:eb3}


In this chapter a dynamic vision sensor will be combined with an active depth-sensor to form the \dedvsK sensor which provides a stream of 3D point events.
This sparse stream of 3D points will be used in the novel \ebslame algorithm which has a very low computational footprint while at the same time provides very good results.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.70\columnwidth]{eb3/teaser/slammer_screenshot_3.png}
	\caption[\ebslame overview]{
		The camera trajectory (red) and the sparse 3D map (shades of grey) are created simultaneously in realtime using only a sparse stream of 3D point events (green).
	}
	\label{fig:eb3_teaser}
\end{figure}

\clearpage



\section{Introduction}
\label{sec:eb3_intro}

In this chapter the previously developed \ebslam algorithm is extended to a full 3D SLAM algorithm.
In contrast to 2D SLAM, a 3D SLAM algorithm creates a map of the three-dimensional environment which is suitable for tracking all six degrees of freedoms of three-dimensional motions.
3D algorithms are much more powerful than their 2D counterparts, for example especially allowing flying robots, but also have a much higher theoretical and computational complexity.

Recent 3D SLAM algorithms \cite{bylow2013rss,endres12icra,huhle2013sparse,kerl13iros,kerl2013robust,newcombe2011kinectfusion} almost exclusively use a 3D sensor like PrimeSense (see \refsec{dasp_intro_rgbd}) to get depth measurements.
Such sensors provide depth measurements in a range of 1-10 meters at an accuracy of 1-20 cm at a framerate of 30 or 60 frames per second.
Additionally, a colour image is provided which may be an essential part in the SLAM algorithm or simply be used to augment the environment map.
% Several important state of the art algorithms are discussed at the end of this section.

KinectFusion \cite{newcombe2011kinectfusion} demonstrated how dense surface mapping and tracking can be accomplished with a Microsoft Kinect combined colour and depth sensor in realtime using GPU accelerated hardware.
The algorithm proceeds in four steps.
First a mesh, i.e. surface vertices and surface normals, of the currently observed surface is reconstructed from the measured depth values.
This current measurement is compared towards the constructed scene model, and an iterated closest point (ICP) algorithm is used to align the mesh towards the model in order to derive the current camera pose in a global coordinate system.
This pose estimate is used to integrate the current surface measurement into the scene model which is represented by a truncated volumetric signed distance function.
This procedure is an example of an iterative approach to SLAM where the current map is used to compute a pose estimate and the new pose estimate is used to update the map.
An evaluation of KinectFusion showed that it can produce detailed and drift-free environment models and that it can run in realtime if supported by GPU hardware.

Endres et al. \cite{endres12icra} present a different method which matches frames based on image feature points from SIFT or SURF, and uses a RANSAC algorithm to compute possible transformations which are further optimized globally using a pose graph optimization like $g^2 o$.
Kerl et al. \cite{kerl2013robust} try to optimize the frame-to-frame change in pose based on a warped representation of colour and depth image space by analytically deriving an optimizing procedure over the lie algebra $\mathfrak{se}(3)$.
Bylow et al. \cite{bylow2013rss} use a similar optimization over $\mathfrak{se}(3)$ to improve frame-to-frame matching using a similar signed distance function like in KinectFusion.

State-of-the-art algorithms have been quite successful in generating detailed dense environment maps, but still required a high amount of computation power -- in many cases in form of dedicated GPU hardware.
In this chapter the benefits of dynamic vision sensors will be exploited to develop an event-based 3D SLAM algorithm which builds a highly efficient sparse environment map \reffig{eb3_pe} and has the same robustness and quality than dense 3D algorithms.
While event-based vision is a novel approach to design efficient algorithms for embedded systems, it has the same fundamental problems as classic computer vision with respect to estimating depth information.
In classic computer vision this problem was solved for now by ignoring it and using active depth sensors.
For the \ebslame (\ebslameK) algorithm the same approach will be used, and the \edvsK is combined with a PrimeSense active depth sensor to provide depth information for events.

\begin{figure}
	\centering
	\fwn{3}
	\includegraphics{eb3/maps/color.jpg}%
	\includegraphics{eb3/maps/eb.jpg}%
	\includegraphics{eb3/maps/mesh.jpg}
	\caption[Sparse map vs dense map]{
		Comparison of sparse event-based map (middle) against a mesh created by the dense mapping algorithm KinectFusion (right).
		The color image (left) is provided for reference.
	}
	\label{fig:eb3_pe}
\end{figure}

Towards this goal an \edvsK sensor is mounted on top of a standard PrimeSense sensor to form a depth-augmented dynamic vision sensor, the \dedvsK.
Both sensors are calibrated such that for each individual event of the \edvsK the corresponding depth can be computed using the depth stream from PrimeSense.
Thus the \ebslameK algorithm directly works on a sparse stream of 3D point events -- compared to the \ebslamoK algorithm which works on a stream of 2D pixel events.
This especially solves the problem of map back-projection stated in \refsec{ebslam_slam_mu}.

Due to the complexity of 3D environments and the memory consumption of dense grid maps, the \ebslameK algorithm will use an optimized data structure for the map and other simplifications over \ebslamoK to assure realtime performance.
In the end the algorithm presents itself in a strikingly simple formulation which can easily be implemented with only a few lines of source code.

This chapter will be continued with a presentation of the construction and calibration of the \dedvsK sensor in \refsec{eb3_dedvs}.
After a presentation and discussion of the \ebslameK algorithm in \refsec{eb3_ebslam3}, the chapter is concluded with an thorough comparison of the proposed \ebslameK algorithm towards ground truth of an external tracking system and the state-of-the-art algorithm KinectFusion in \refsec{eb3_eval}.



\section{The \dedvsK sensor}
\label{sec:eb3_dedvs}

To build the \dedvsK, an \edvsK and a Asus Xtion camera using the PrimeSense sensor are combined physically by designing and constructing a small camera mount with the aid of a laser cutter (see \reffig{eb3_calib_ray}).
The whole construction is fastened onto the aluminium casing of the Asus Xtion and is quite robust to external forces.
For evaluation purposes an additional modular mount for markers used by the external camera tracker is built.
The lens for the \edvsK sensor was chosen to match the vertical field of view of the depth sensor.
This implies that the horizontal field of view of PrimeSense is not fully covered by the \edvsK as the aspect ratio of the \edvsK and the depth sensor are different.

\begin{figure}
	\centering
	\fwn{2}
	\hfill
	\includegraphics{eb3/dedvs_photo/dedvs.jpg}
	\hfill
	\includegraphics{eb3/dedvs_photo/dedvs_gt.jpg}
	\hfill
	\caption[The \dedvsK sensor]{
		The \dedvsK: The \edvsK is mounted on to of an Asus Xtion using a laser cutted casing ({\bf left}) and for ground truth recordings an additional mount for markers can be added on top ({\bf right}).
	}
	\label{fig:eb3_calib_ray}
\end{figure}

In general a pinhole camera model is defined by the following equation \refeq{eb3_sensor} which describes the projection of a 3D point $p \in \reals^3$ onto the image sensor.
\begin{equation}
	\mu\left(p\,|\,f,c,\kappa_1,\kappa_2\right) := L\left(\|s\|\,|\,\kappa_1,\kappa_2\right)^{-1} \, s + c \, \text{ with } \, s = \frac{f}{p_z} \, \begin{pmatrix} p_x \\ p_y \end{pmatrix}
	\label{eq:eb3_sensor}
\end{equation}
The camera model has several parameters:
$f$ the focal length parameter, $c$ the optical centre of the projection and $\kappa_1,\kappa_2$ for a simple but sufficient lens distortion model:
\begin{equation}
	L(r\,|\,\kappa_1,\kappa_2) := 1 + \kappa_1 \, r + \kappa_2 \, r^2
	\label{eq:eb3_lens}
\end{equation}
When depth values for pixel coordinates are available, the projection can be undone by inverting the projection:
\begin{equation}
	\mu^{-1}\left(u,D\,|\,f,c,\kappa_1,\kappa_2\right) := \frac{D}{f} \, \begin{pmatrix} s_x \\ s_y \\ f \end{pmatrix} \, \text{ with } \, s = L\left(\|u-c\|\,|\,\kappa_1,\kappa_2\right) \, (u - c)
	\label{eq:eb3_sensorinv}
\end{equation}

When a PrimeSense and an \edvsK sensor are combined one can take a pixel coordinate $u$ on the depth sensor with corresponding depth value $D$ and compute the corresponding pixel coordinate $v$ on the \edvsK sensor by combining equations \refeq{eb3_sensor} and \refeq{eb3_sensorinv}:
\begin{equation}
	v = f(u,D\,|\,R,t,P_\text{PS},P_\text{eDVS}) := \mu\left(R \, \mu^{-1}\left(u,D\,|\,P_\text{PS}\right) + t \,|\,P_\text{eDVS}\right)
\end{equation}
Here $P_\text{PS}$ resp. $P_\text{eDVS}$ are the set of camera parameters of the PrimeSense resp. \edvsK sensors and $R \in \reals^{3\times 3}$ and $t \in \reals^3$ are the spatial transformation between the two sensors.

The camera parameters and the transformation can be found by numerical optimization of the non-linear least-square problem
\begin{equation}
	% F\left( (u_i,D_i)_{1\le i \le n}, (v_i)_{1\le i \le n} \,|\, R, t, P_\text{PS}, P_\text{eDVS} \right) :=
	\min_{R,t,P_\text{PS},P_\text{eDVS}}\,\sum_{i=1}^n{\left\| v_i - f(u_i,D_i\,|\,R,t,P_\text{PS},P_\text{eDVS})\right\|^2}
\end{equation}
for a set of measured data points $(u_i,D_i,v_i)_{1\le i \le n}$ over the parameters $R, t, P_\text{PS}, P_\text{eDVS}$.
It has to be noted that the two focal length parameters are coupled together and it only makes sense to optimize them with respect to each other.
Thus the focal length parameter for the PrimeSense sensor was fixed to the reference value of $520$.

As the two cameras are very close together and approximately point into the same direction, the identity transformation is a good initial guess for the transformation parameters $R$ and $t$.
Initial values for the focal length projection parameters can be estimated from the opening angle and initial values for the barrel distortion are chosen as $0$.
The projection centre parameters can be initialized well by choosing the optical centre.
This results in the initial parameters
\begin{equation}
	R_0 = I,\, t_0 = 0,\, P_{\text{PS},0} = (520, (320,240), 0, 0),\, P_{\text{eDVS},0} = (160, (64,64), 0, 0)
\end{equation}

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{eb3/calib/eb3_calib.png}
	\caption[\dedvsK calibration]{
		{\bf Left:} The corners of a calibration plate are tracked with OpenCV (light blue) and the position of the diode blue is computed.
		{\bf Middle:} The calibration plate viewed by the \edvsK.
		{\bf Right:} Events after frequency filtering and the computed position of the diode (red).
	}
	\label{fig:eb3_dedvs_calib}
\end{figure}

Data points for the least square problem are captured using a calibration plate and a diode emitting a pulsed stream of light.
Classic camera calibration methods use a calibration plate, however it is not straight-forward to track a calibration plate in a dynamic vision sensor and up to now no standard algorithms or software libraries are available.
Dynamic vision sensors can be handled much easier by using a temporally changing signal like a pulsed diode.
Thus a combined approach is used where a single calibration point is tracked in the PrimeSense image using a standard calibration plate.
In the calibration plate a pulsed diode was inserted which can be easily detected and tracked with a dynamic vision sensor (\reffig{eb3_dedvs_calib}).
Here we exploit the fact that the pulse frequency is known and filter out all events which do not correspond to the fixed pulse frequency.
This method reliably filters out all events which are due to the movement of the calibration plate and only preserves events from the diode (\reffig{eb3_dedvs_calib}).

With sufficient training points and due to the small deviation from the initial parameters a local optimization is sufficient for optimizing the camera parameters up to a root mean square error of $0.9$ pixels in \edvsK coordinates -- the minimal possible error being $0.25$ pixels due to rounding errors.

The optimized camera parameters can be used to compute an \edvsK pixel coordinate for a given PrimeSense pixel coordinate with corresponding depth value.
However to annotate \edvsK pixel events with depth values the opposite direction is required.
An \edvsK pixel coordinate ray manifest as a short line segment when back-projected onto the PrimeSense sensor.
The desired depth value lies on this line, but the correct value has to be computed.
% \begin{figure}
% 	\centering
% 	\includegraphics[height=0.45\textwidth]{eb3/calib_ray/edvs.pdf}
% 	\includegraphics[height=0.45\textwidth]{eb3/calib_ray/primesense.pdf}
% 	\caption[\edvsK and PrimeSense correspondence]{
% 		{\bf Left:} Events from \edvs.
% 		{\bf Right:} Rays (light blue) from \edvsK pixel events (dark blue) projected into depth-image (grey value indicates depth). The desired event depth can can be found by computing the intersection of the event ray with the depth image.
% 	}
% 	\label{fig:eb3_dedvs_rays}
% \end{figure}
\Reffig{eb3_dedvs_depth} shows a top-down schematic view of this problem.
An \edvsK event ray (blue) hits several PrimeSense pixel rays (black) and the correct intersection with the measured depth surface (grey) needs to be found.
This problem is solved by pre-generating a look-up table where for each possible event ray pre-computed pixel coordinates together with depth values $d_i$ (blue dots in figure) are stored.
For each event during execution, these are compared to actual depth values $D_i$ (black dots in figure) to find intersections, i.e. an index which satisfies $d_i \le D_i \le D_{i+1} \le d_{i+1}$.
In case of multiple intersection, the nearest has to be taken.
As the line segment intersection takes place between singular pixels, the desired depth is computed by simply averaging the selected neighbouring depth values:
$D_\text{final} = \frac{1}{2}(D_i + D_{i+1})$.

\begin{figure}
	\centering
	\includegraphics[height=0.32\textwidth]{eb3/dedvs_depth/dedvs_depth.pdf}
	\hspace{1cm}
	\includegraphics[height=0.32\textwidth]{eb3/dedvs_depth/dedvs_depth_lines.pdf}
	\caption[Event pixel rays and depth computation]{
		{\bf Left:} An event pixel ray (cyan) from the \edvsK sensor (blue) crosses several pixels from the PrimeSense depth sensor (black).
		The correct event depth value (red) is computed by intersecting the event ray with the measured depth surface (gray).
		{\bf Right:} Close up.
	}
	\label{fig:eb3_dedvs_depth}
\end{figure}

\edvsK pixel coordinates and depth values can be used to compute corresponding 3D points in camera coordinates using again \refeq{eb3_sensorinv}.
The result of calibration and event depth computation is an event-based dynamic vision sensor which generates a stream of 3D point events.
\Reffig{eb3_dedvs_stream} shows an example stream of depth-annotated events where the depth is encoded by colour.
It is clearly visible how depth values further increase the information content of the event-stream.

\begin{figure}
	\centering
	\fwnfull
	\includegraphics{eb3/dedvs_stream/stream.png}
	\caption[Example of \dedvsK stream]{
		Stream of \edvsK pixel events annotated with depth values (colour red to blue encodes distance near to far).
		Each images shows 1500 events and between images 30000 events are skipped.
	}
	\label{fig:eb3_dedvs_stream}
\end{figure}



\section{The Event-based 3D SLAM algorithm}
\label{sec:eb3_ebslam3}

The event-based SLAM algorithm described in \refsec{ebslam} builds a map based on the relative occurrence of events in specific map locations and uses a particle filter to continuously update an estimate of the camera pose.
While the algorithm was only applied in the 2D case so far (see \refsec{ebslam_app_2d} and \refsec{ebslam_app_explore}), it is formulated in a general way which in theory allows a direct application to the 2.5D or 3D case.
The main challenge lies in the fact that the mapping between map locations and image coordinates is no longer a one-to-one mapping, but a ray through an image pixel hits a multitude of possible map locations.
While the occurrence based approach could theoretically still be realized by using the whole pixel event ray and updating all corresponding pixels in the map which are hit by the ray, this approach is not investigated further here due to its slow runtime and possible instability.
Instead the \dedvsK sensor which directly generates a stream of 3D point events is used to solve the event unprojection problem.

Following the notation from \refsec{ebslam_slam_mu}, we have $\dwsdom=\seg{3}$ for the state space and $\dwmdom = \reals^3$ for the map space.
Remember that the state space denotes the set of possible camera poses which is now a 3D rotation and a 3D position -- thus described by the special Euclidean group -- and the map space is space over which the map is built.
By using the \dedvsK sensor, each 2D pixel events has a corresponding 3D point -- denoted with $\dweddom := \reals^3$.
However pixel coordinates are not required in the following, thus $\dweddom$ will be used as event space.
The projection function $\mu$ can be simplified to
\begin{equation}
	\mu : \dwmdom \times \dwsdom \rightarrow \dweddom,\, \mu(u\,\vert\,p) = p \, u := p_R \, u + p_t
	\label{eq:eb3_mu}
\end{equation}
where $p$ is treated as an Euclidean transformation defined by rotation $p_R \in \sog{3}$ and translation $p_t \in \reals^3$.
Now the inverse of $\mu$ is well defined as
\begin{equation}
	\mu^{-1} : \dweddom \times \dwsdom \rightarrow \dwmdom,\, \mu^{-1}(e\,\vert\,p) = p^{-1} \, e = p_R^{-1} \, (e - p_t)
	\label{eq:eb3_muinv}
\end{equation}

The \ebslamK algorithm is using three maps:
The occurrence map $\dwmo$, the normalization map $\dwmz$ and the final map $\dwmm$.
With the projection function $\refeq{eb3_muinv}$, the occurrence map from \refeq{ebslam_O} can directly be extended to 3D:
\begin{equation}
	\wt{\dwmo}(u) = \wtp{\dwmo}(u) + \sum_{i=1}^{n}{\wt{s_i} \, \dwrndn\left(u \,\middle|\, (\wt{p_i})^{-1} \wt{e}, \sigma\right)}\,, \; \wtz{\dwmo} = 0 \,.
	\label{eq:eb3_O}
\end{equation}
For the 2D occurrence map pixel events where integrate in a 2D grid and now for the 3D occurrence map 3D point events are integrate in a 3D grid -- also called voxel grid.

The standard deviation $\sigma$ in \refeq{eb3_O} can be derived from the distance $D(e)$ of an event $e \in \dweddom$ to the camera and the pixel focal length of the event-based sensor as
\begin{equation}
	\sigma(e) := \frac{D(e)}{2 \, \lambda \, f} \,
	\label{eq:eb3_O_sigma}
\end{equation}
$\lambda$ scales the normal distribution such that a given percentage of samples would be projected within one pixel on the \edvsK sensor.
A value of $\lambda = 2$ would indicate ''$2 \, \sigma$'' thus $95\%$.
Using a normal distribution is not an exact sensor model but a reasonable good approximation and values of $\lambda=1.5$ have proven to be a good choice.

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{eb3/series/series_1.png}
	\caption[\ebslameK map and path over time]{
		Computation of sparse 3D map (shades of grey) and path (red) over time for an example scenario.
		Current events are displayed as green dots.
	}
	\label{fig:eb3_series}
\end{figure}

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{eb3/maps/scn4_take05.pdf}
	% \includegraphics{eb3/maps/scn4_take05_1.png}\\
	% \includegraphics[width=0.45\textwidth]{eb3/maps/scn4_take05_col1.png}
	% \includegraphics[width=0.45\textwidth]{eb3/maps/scn4_take05_col2.png}%
	\caption[\ebslameK map and path over time]{
		Sparse 3D map created by \ebslame and photos of the same scenario from a different viewpoint.
		Corresponding key features are connected with lines for orientation.
	}
	\label{fig:eb3_maps}
\end{figure}

\Reffig{eb3_series} shows an example run of the algorithm and demonstrates how path and map are created over time for an example scenario.
The map itself is a sparse representation of the environment which unlike a mesh provided by dense methods is not necessarily directly accessible to the human eye.
On closer inspection however it is visible how especially geometry borders and texture edges are represented in the event-based map.
As another example, \reffig{eb3_maps} shows a bigger map for an office environment in comparison with colour photos.
It is important to note that the map is not generated for direct visual processing by humans, but serves only the purpose of self-localization in the environment with the \ebslame algorithm.

The final algorithm is listed in pseudo-code in \refalg{ebslame_main}.
\ebslame can be implemented with only a few lines of code which is very short for a 3D SLAM algorithm, additionally no massively parallel operations are necessary.
One can see, that the normalization from \ebslamoK is not performed and the main reason is, that the normalization map is computational very intensive, can lead to instabilities and is actually quite smooth so it does not have the same impact as the occurrence map.
More details are explained in the following considerations.
The motion model is again realized as a diffusion process using a covariance matrix which models the possible movement for one event.
Funnily enough, the particle resampling is actually the most complex operation.
In the following several topics related to the \ebslame algorithm are investigated in more detail.

\begin{algorithm}[!h]
	\caption{\ebslame (\ebslameK)}
	\label{alg:ebslame_main}
	\begin{algorithmic}
		\State $\forall \, u \in \dwmdom: \dwmo(u) = 0$
		\State $\forall \, 1 \le i \le N: p_i = 0, s_i = 1, p_{*} = 0$
		\For{\text{each new point event} $e$}
			\For{$i=1 \to N$}
				\State Sample $p_i$ from $\dwrndn_{\seg{3}}(p_i,\Sigma)$
				\State $s_i = (1 - \alpha) \, s_i + \alpha \, \dwmo(p_i^{-1} \, e)$
			\EndFor
			\For{$i=1 \to N$}
				\State $\dwmo = \dwmo + \dwrndn\left(\cdot\,\vert\,p_i^{-1} \, e,\sigma\right)$
			\EndFor
			\If{every K-th event}
				\State $p_{*} = \frac{\sum_{i=1}^N{s_i \, p_i}}{\sum_{i=1}^N{s_i}}$
				\State $\text{execute particle resampling}$
			\EndIf
		\EndFor
	\end{algorithmic}
\end{algorithm}

\subsubsection{Map normalization}

The normalization map requires a continuous update of visible voxels to provide a relative measurement of how often a voxel has actually generated an event given the number of possibilities (see \refeq{ebslam_M}).
For a three-dimensional map this requires to update all voxels which are in the current view frustum.
To avoid extensive visibility computations for individual voxels, the normalization map could be subsampled and the values for individual voxels could be computed using linear interpolation.
However as already apparent in \reffig{ebslam_map}, the normalization map is much smoother than the occurrence map.
The occurrence map capture thin edges, singular points and other filigree features, and can thus change its value rapidly over a distance of only a few voxels.
On the contrary, the normalization map captures if a voxel is visible, and as the frustum is normally quite wide in number of voxels the value of the normalization map as a function of space changes only slowly.
This results in the important observation that for a given event and several similar particle poses the occurrence map has a much higher influence on the relative difference between particle scores than the normalization map.
This is in general not true for a given pose and different events, but in a particle filter particles are only evaluated and selected relative to each other with respect to one measurement after another.
If for the current event the local values in the occurrence map are generally low compared to other regions which have been observed longer, scores will be low for all particles and all particles will be subject to the same selection pressure.
On the opposite if the values in the occurrence map are generally high because a lot of information has already been gathered, all particles will have this benefit and again all particles have the same chance to increase their score if they align well with the map.

\subsubsection{Parametrization}

The algorithm can be parametrized in several ways which will be explained in the following.
In \refsec{eb3_eval} an evaluation of theses parameters is presented and their impact on the tracking quality and the performance of the algorithm are qualitatively evaluated.

\begin{description}
\item[Particle count $N$:] The number of particles used in the particle filter has a strong impact on the quality of the result path but also a direct negative impact on the runtime. 
\item[Events until resampling $K$:] Resampling after every event has a huge impact on performance and yields much worse results. This parameter delays the resampling step after enough information has been gathered. 
\item[Exponential decay factor $\alpha$:] This constant can be computed conveniently with \refeq{ebv_alpha}. The constant $G_0$ is chosen to be K in the following -- this has a reasonable interpretation and gives good results.
\item[Batch size $B$:] Batching as explained in \refsec{ebv_pf_algo} can be used also for \ebslame. This parameter has a huge impact on performance for small values of $B$. A good choice is $B=3$. 
\item[Event share:] An additional trick to reduce runtime is to not use all events, but only a random subset. This directly increased performance but if too many events are discarded tracking quality suffers.
\item[Diffusion:] As \ebslame uses random diffusion as a motion model and the movement per event is much more difficult to compute, the standard deviation for position and rotation diffusion should be chosen wisely.
Evaluation will show that a range of values works well for a broad selection of scenarios.  
\end{description}


\subsubsection{Voxel map}

As the map space is three-dimensional, a three-dimensional grid map of voxels has to be maintained for the occurrence map.
The data structure for the voxel grid has to be chosen with care to avoid exessive memory consumption and slow access times.
On the one hand a fast access mechanism is crucial to the runtime of the \ebslamK algorithm as the map has to be updated for all events and several particles.
On the other hand dense voxel grids require a huge amount of memory.
A voxel grid which stores a 4 byte floating point value at each voxel and with has a side length of 512 voxels already requires 512 MB system memory -- compare to a 2D grid with $512^2$ pixels which only requires 1 MB.

A typical data structure for efficiently storing voxels is an octree.
An octree covers a rectangular region of space by recursively splitting it into eight equally sized and axis aligned boxes down to the level of individual voxels.
Only nodes which actually contain a non-zero voxel are created, thus saving memory for all voxels which are not used.
Octrees have the disadvantage, that the covered space has to be known in advance and that access time is logarithmic in the side length of the covering box.
For a box with side length of 10 meters and a resolution of 1 cm, this constant is already 10.
Additionally as space is partitioned down to individual voxels and new nodes are created on-the-fly, memory access is not streamlined.
Here a two-level approach similar to \cite{web:openvdb} is used, where the octree is only divided up to a specific size and dense ''chunks'', i.e. voxel grids of size $32^3$, are used as leaf nodes instead of individual voxels.
\Reffig{eb3_chunks} shows an example of such a chunk voxel grid.
In this example there are only 316 out of 1664 chunks filled with voxels saving 80\% of memory.
This ratio usually gets much higher the longer the SLAM process is running.

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{eb3/chunks/chunks.png}
	\caption[Chunk voxel grid]{
		Visualization of voxel grid ''chunks''.
		Spheres coloured blue to red indicate centre position of chunks which contain at least one voxel.
		Chunks with a significant amount of voxels are rendered as grey boxes.
		The large box indicates the area covered by the whole voxel grid.
		Additionally the estimated path (red/orange), current events (green) and the current map (shades of grey) are displayed.
	}
	\label{fig:eb3_chunks}
\end{figure}


\subsubsection{Diffusion}

The diffusion model $\dwrndn_{\seg{3}}(p,\Sigma)$ for a 3D pose $p \in \seg{3}$ can actually be a bit tricky.
It requires to diffuse a three-dimensional position $t \in \reals^3$ and a three-dimensional rotation $R \in \sog{3}$.
For the position this is straight forward and realized as
\begin{equation}
	\left(x,y,z\right) \leftarrow \left( \dwrndn(x,\sigma_x), \dwrndn(y,\sigma_y), \dwrndn(z,\sigma_z) \right)
\end{equation}
where $\dwrndn$ is a Gaussian normal distribution.

However, for a rotation the diffusion process is by no means straight forward \cite{nikolayev1997normal}.
A two-dimensional rotation has only one degree of freedom, the angle of rotation, and one could think of taking a normally distributed angle to create a normally distributed rotation matrix:
\begin{equation}
	R \leftarrow  R_2(\dwrndn(0,\sigma_\theta)) \, R
\end{equation}
where $R_2(\theta)$ is the 2D rotation matrix of angle $\theta$.
As $\sog{2}$ can be parametrized by $\reals / [0,2\pi[$, it is identical to $\theta \leftarrow \dwrndn(\theta,\sigma_\theta)$.
This approach has the issue that the two tails of the normal distribution wrap around at an angle of $\pi$ which may be desired or not.
But for \ebslamK this is not much of an issue as $\sigma_\theta$ is usually very small.

For three-dimensional rotations there are two additional degrees of freedom for the direction of rotation.
A possible representation is axis/angle where a rotation is given by an unit vector for the axis of rotation and an angle.
A normally distributed rotation can be sampled by sampling a random, uniformly distributed point on the 2-sphere as the axis and a half-normally distributed angle of rotation.
The axis/angle representation can then be transformed to a rotation matrix.
A random point on a 2-sphere can for example be samples with
\begin{equation}
	a \leftarrow \frac{\left(x,y,z\right)}{\sqrt{x^2+y^2+z^2}}
\end{equation}
where $x,y,z$ are sampled from a standard normal distribution $\dwrndn(0,1)$.
Given a uniformly distributed axis $a \in \reals^3$ and a normally distributed angle $\theta \leftarrow \dwrndn(\theta,\sigma_\theta)$, a normally distributed quaternion can be computed as
\begin{equation}
	\left(\cos\tfrac{\theta}{2},\,\sin\tfrac{\theta}{2}\,a_x,\,\sin\tfrac{\theta}{2}\,a_y,\,\sin\tfrac{\theta}{2}\,a_z \right)
\end{equation}
and for small angles this can be approximated with:
\begin{equation}
	\left(1-\tfrac{\theta^2}{4},\,\tfrac{\theta}{2}\,a_x,\,\tfrac{\theta}{2}\,a_y,\,\tfrac{\theta}{2}\,a_z \right)
\end{equation}



\section{Evaluation}
\label{sec:eb3_eval}


In order to compare paths from different trackers, theses paths first needs to be aligned towards each other.
This step is necessary as most tracking algorithms choose an arbitrary global coordinate system for map and path and thus a transformation between the two systems need to be computed.
A path is a sequence of tuples $u = (t,p,q) \in \reals \times \reals^3 \times \sog{3} =: U$, where $p \in \reals^3$ indicates the current position, $q \in \sog{3}$ the current rotation represented by a quaternion, and $t \in \realspos$ the corresponding timestamp of the pose.
Timestamps greatly simplify the alignment process as a point on one path only needs to be matched against the temporal nearest point on the other path.
Thus the root-mean-square error function (RMSE) of two paths $(u_i)$ and $({u'}_j)$ is defined as:
\begin{equation}
	E\left((u_i),({u'}_i)\,\vert\,a\right) := \sqrt{\tfrac{1}{n} \sum_{i=0}^n{\left(e(A(u_i,a), {u'}_{\mu(j)}\right)^2}}
\end{equation}
where the matching function $\mu : \mathbb{N} \rightarrow \mathbb{N}$ is defined as $\mu(i) := \argmin_{j}{|t_i-{t'}_j|}$.
Other matching mechanisms are possible, in particular it may be advantageous to reject points at the ends of the paths when the minimal temporal difference is too high.
Here the concrete error function $e$ measures the Euclidean distance between the tracked path point and the ground truth path point:
\begin{equation}
	e(u,u') := \| p - p' \|
\end{equation}
The alignment function $A : U \times U \rightarrow U$ applies the transformation of the parameter set $a$ on a point on the path and is defined as
\begin{equation}
	A\left((t,p,q),(t_a,p_a,q_a)\right) := (t + t_a, q_a \, p + p_a, q_a\,q)
\end{equation}
The alignment includes a temporal offset $t_a$ as the two data streams can be started at slightly different times.
The optimal alignment
\begin{equation}
	a^{*} = \argmin_{a \in U}{E\left((u_i),({u'}_i)\,\vert\,a\right)}
\end{equation}
can be found by a mathematical optimization.
Possible methods are gradient descent or Particle Swarm Optimization (PSO) \cite{kennedy1995particle}, or if the temporal offset is neglected a principal component analysis can be used.
For the evaluation in this section accelerate particle swarm optimization \cite{yang2011accelerated} was used which could find very good alignments in a matter of seconds.

Additionally it is possible to consider the rotational alignment error
\begin{equation}
	e_\text{rot}(u,u') := |2\cos^{-1}\left((q^{-1}\,q')_w\right)|
\end{equation}
This definition of $e_\text{q}$ is due to the fact that the $w$ component of a unit quaternion directly encodes the amount of rotation.
The rotational error additionally requires the alignment of the orientation as the coordinate frame chosen by for example an overhead tracker is not necessarily identical to the implicit camera coordinate frame where the x- and y-axis define the image plane and the z-axis points into the world.

For evaluation a dataset of 26 ''takes'', i.e. paths, in five different scenarios and the length of individual takes was 20 to 40 seconds.
\Reffig{eb3_scns} shows colour images for an impression of several scenarios.
To compare the tracking results against ground truth the pose of the camera was tracked with the marker-based overhead tracking system OptiTrack V100:R2.
Due to the limitations of the PrimeSense depth sensor and the fact that the overhead tracker is immovable, room scenarios are limited to one room indoor settings.
For this dataset, the depth information was computed from the high-resolution depth stream with a spatial resolution of 640x480 and a framerate of 30 Hz, with the exception of the scenario ''Table 1'' which was recorded with a resolution of 320x240 and a framerate of 60 Hz.
While the spatial resolution does not have an impact on the results of \ebslame as it is downsampled to the \edvsK sensor resolution of 128x128 pixels, the framerate has a large impact.
With a framerate of 60 Hz the PrimeSense sensor provides reliable depth information only in a range up to approximately 1.5 m as it uses a low-resolution infrared image to compute depth.
With the lower framerate of 30 Hz depth measurement are accurate up to 2.5 m.

\begin{figure}
	\centering
	\fwn{3}
	\includegraphics{eb3/rmse/scn1_col.jpg}%
	\includegraphics{eb3/rmse/scn7_col.jpg}%
	\includegraphics{eb3/rmse/scn8_col.jpg}
	\caption[The scenarios for this evaluation]{
		Colour images for some of the scenarios used in this evaluation.
		From left to right: Scenarios ''Table 1'', ''Room'' and ''People''.
	}
	\label{fig:eb3_scns}
\end{figure}

The \ebslameK algorithm was executed with two sets of parameters: The ''default'' set uses a reasonable choice of parameters which achieves very good tracking results and the ''fast'' parameter set uses parameters which focus on very fast execution speed with only small impact on the RMSE.
\Reftab{eb3_rmse} shows an overview of the RMSE and the processing speed for the different scenarios.
The RMSE is excellent when considering the complexity of the 3D matching problem and the low resolution of the \edvsK sensor.
The processing speed is measured as the ''relative realtime factor'' which is the quotient between recording time and computation time.
This is necessary as the rate at which events are generated depends on movement speed and visual complexity of the scene.
However all takes are recorded with more or less constant movement and normal velocities with no particular breaks to improve tracking performance.

\Reffig{eb3_paths} shows two examples for paths tracked with \ebslameK compared against ground truth from an overhead tracking system.
Six more examples are shown in the appendix in \reffig{apx_eb3_paths}.
The plots demonstrates the very good results which can be achieved with \ebslameK.

To give an insight into the influence of parameters of the \ebslame algorithm, RMSE and runtime performance were measured for a variety of different parameter choices.
\Reffig{eb3_sweeps} shows the RMSE and relative speed of for varying number of particles $N$ and varying number of events until resampling $K$ averaged over the whole dataset.
Results for additional parameters are shown in the appendix in \reffig{apx_eb3_sweeps_1} and \reffig{apx_eb3_sweeps_2}.
For this evaluation, values for the non-modified parameters were chosen equal to the ''default'' parameter set: $N=100$, $K=100$, $B=3$, $100\%$ used events, voxel size $0.01$ cm and standard deviation for position and rotation diffusion equal to $0.008$.

\newcolumntype{R}{>{\raggedleft\arraybackslash}m{1.3cm}}
\newcolumntype{M}{>{\raggedleft\arraybackslash}m{1.7cm}}

\begin{table}[h!]
	\caption[Memory consumption, RMSE and speed for \ebslame]{
		Memory consumption, positional root-mean-square error (RMSE) and runtime performance for \ebslameK for the different scenarios.
	}
	\small
	\centering
	\begin{tabular}{lc|M|RR|RR}
		\toprule
		\multirow{2}{*}{Scenario} & \multirow{2}{*}{Takes} & \ebslamK & \multicolumn{2}{c|}{\ebslamK (default)} & \multicolumn{2}{c}{\ebslamK (fast)} \\
		 & & RAM & RMSE & Speed & RMSE & Speed \\
		\midrule
		1: ''Table 1''   & 2 & 25 MB &  3.1 cm & 2.0 x &  4.0 cm & 20 x \\
		2: ''Sideboard'' & 4 & 14 MB &  4.0 cm & 2.2 x &  5.2 cm & 23 x \\
		3: ''Table 2''   & 8 & 27 MB &  4.9 cm & 1.4 x &  9.1 cm & 16 x \\
		4: ''Room''      & 8 & 21 MB & 13.4 cm & 2.5 x & 13.3 cm & 27 x \\
		5: ''People''    & 4 & 15 MB &  6.1 cm & 2.2 x &  7.0 cm & 24 x \\
		\bottomrule
	\end{tabular}
	\label{tab:eb3_rmse}
\end{table}

\clearpage

\begin{figure}
	\centering
	% \fwn{2}
	\includegraphics[width=7cm]{eb3/paths/scn1_take02.png}%
	\includegraphics[width=7cm]{eb3/paths/scn2c_take02.png}
	\caption[Path comparison for \ebslameK]{
		Two examples trajectories generated by \ebslameK (red) compared to ground truth (blue) from an overhead tracking system.
		The left path is from scenario 1 and the right path from scenario 2.
	}
	\label{fig:eb3_paths}
\end{figure}

\begin{figure}
	\centering
	\fwnfull{}
	\includegraphics{eb3/sweeps/numParticles.pdf}\\
	\includegraphics{eb3/sweeps/resampleK.pdf}
	\caption[RMSE and runtime for \ebslameK]{
		RMSE in position and execution time relative to take duration plotted for the following parameters:
		Number of particles $N$ (top) and number of events until resampling $K$ (bottom). 
	}
	\label{fig:eb3_sweeps}
\end{figure}
