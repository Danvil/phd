%!TEX root = phd.tex


\chapter{Partition quality metrics}
\label{sec:apx_metric}



\section{Quality metrics for superpixels}
\label{sec:apx_metric_sp}


Superpixel properties can be divided in two groups of metric: supervised and unsupervised.
Supervised metrics compare a superpixel segmentation against manually created ground truth.
This includes the boundary recall measure (see \refdef{dasp_sp_props_br}) and the undersegmentation error (see \refdef{dasp_sp_props_use}).
Unsupervised metrics measures intrinsic properties of superpixels and do not require manual ground truth.
In this section are the following metrics: isoperimetric quotient (see \refdef{dasp_sp_props_ipq}), superpixel connectivity quotient (see \refdef{dasp_sp_props_cq}), explained variation (see \refdef{dasp_sp_props_ev}) and compression error (see \refdef{dasp_sp_props_ce}).

In the following it is assumed that $\dwp$ is a partition of a finite undirected graph.
$\partial\,S$ indicates the {\bf boundary} of a segment $S \in \dwp$ in the sense of \refeq{sdasp_boundarysegment} and \refeq{sdasp_boundarypartition}.
$|\partial S|$ denotes the {\bf length of the boundary}, i.e. the number of vertices on the boundary of the segment $S \in \dwp$, and $|S|$ the area of the segment, i.e. the number of vertices belonging to the $S$.
The graph structure can for example be the regular pixel lattice graph or the irregular neighbourhood graph of superpixels.

\subsubsection{Boundary Recall}

Boundary recall measures how well the boundary of a partition matches the segment boundary of a given reference partition.
It indicates how much of the boundary is superfluous or missing with respect to the reference.

\begin{mydef}
\label{def:dasp_sp_props_br}
	Let $\dwp$, $\dwpb$ be partitions.
	The {\bf boundary recall (BR)} \index{boundary recall} of $\dwp$ with respect to $\dwpb$ and a maximum reach $\delta \in \realspos$ is defined as
	\begin{equation}
		\textbf{BR}_{\delta}(\dwp|\dwpb) := \frac{1}{|\partial\,\dwp|} \sum_{v \in \partial\,\dwp}{{\bm 1}_\mathbb{B}(\min_{x \in \partial \dwpb}\|v-x\| < \delta)}
	\end{equation}
\end{mydef}

The Boolean indicator function ${\bm 1}_\mathbb{B}$ is defined as:
\begin{equation}
{\bm 1}_\mathbb{B} : \{\text{True},\text{False}\} \rightarrow \{0,1\},\; {\bm 1_\mathbb{B}}(x) :=
	\begin{cases} 
	1 &\text{if } x = \text{True}, \\
	0 &\text{if } x = \text{False} \,.
	\end{cases}
\end{equation}

The parameter $\delta$ controls how close the boundaries of the two partition must be to one another.
For example, a value of 1 pixel indicates that every pixel of a partition border must be adjacent to a border pixel of the reference partition.
The boundary recall value lies between 0 (worst) and 1 (best) and normally increases with the number of segments.

\subsubsection{Undersegmentation Error}

The undersegmentation error \cite{achanta2010} measures how well segments from a reference partition are matched by segments from a given partition.
For each reference segment all overlapping segments from the given partition are found and the area of theses segments are compared to the size of the reference segment.
The smaller the difference, the better a partition represents the reference partition.

\begin{mydef}
\label{def:dasp_sp_props_use}
	Let $\dwp,\dwpb$ be partitions and $\gamma \in [{0,1}]$ a parameter.
	The {\bf undersegmentation error (USE)} \index{undersegmentation error} is defined as
	\begin{equation}
	\textbf{USE}_{\gamma}(\dwp|\dwpb) :=
		\frac{1}{|\dwp|}\,
		\sum_{T \in \dwpb}{
			\sum_{S \in \dwp}{
				\,\area(S) \, 1_\mathbb{B}\left[\frac{|S \cap T|}{|S|} > \gamma\right]
			}}
		 - 1
	\end{equation}
\end{mydef}

The parameter $\gamma$ accounts for small errors in the boundary computation, a typical value is $\gamma = 0.05$.
The undersegmentation error lies between 0 (best) and 1 (worst) and normally decreases with the number of segments.

\subsubsection{Connectivity}

For a lot of applications it is advantageous that superpixels are connected in the normal graph theoretic sense.

\begin{mydef}
	A segment $S \in \dwp$ is called {\bf connected} if for each two vertices $u,v \in S$ there is a path from $u$ to $v$ which lies completely in $S$.
	A partition $\dwp$ is called {\bf connected} if every segment $S \in \dwp$ is connected.
\end{mydef}

Connectivity is sometimes a very strong assumption, as it forbids small, noisy enclaves.
On the other side, often the criterion is too weak as it does not forbid stretched and widely distributed segments with narrow transitions.

In the following we will use a measure which compares the area of all connected components except the largest against the area of the segment.
\begin{mydef}
\label{def:dasp_sp_props_cq}
	Let $S \in \dwp$ a segment and $\{c_i\} \subset S$ its connected components.
	The {\bf connectivity quotient (CQ)} of a segment $S \in \dwp$ is defined as
	\begin{equation}
		\textbf{CQ}(S) := 1 - \frac{\max_{i}{|c_i|}}{|S|}
	\end{equation}
\end{mydef}
Using the area weighted mean this definition can be extended to the whole partition:
\begin{mydef}
	Let $\dwp$ be a partition of a graph $G = (V,E)$.
	The {\bf connectivity quotient (CQ)} of a partition is defined as
	\begin{equation}
		\textbf{CQ}(\dwp) := \frac{1}{|\dwp|} \sum_{S \in \dwp}{|S|\,\textbf{CQ}(S)}
	\end{equation}
\end{mydef}

In the best case, where all segments are fully connected, the connectivity quotient is $0$.

\subsubsection{Isoperimetric quotient}
\label{sec:apx_metric_sp_ipq}

Superpixels can have many shapes, but a local compact shape is benefitial for many applications.
An easy method to compute the ''compactness'' of superpixels is the isoperimetric quotient.

In a classic, geometric sense, the isoperimetric quotient compares the area of a shape to its perimeter.
The isoperimetric quotient is motivated from Euclidean geometry where it expresses similarity with a circle.
\begin{mydef}
\label{def:dasp_sp_props_ipq}
	The {\bf isoperimetric quotient (IPQ)} of a segment $S$ is defined as
	\begin{equation}
		\textbf{IPQ}(S) := \frac{4 \pi \, |S|}{|\partial S|^2}
	\end{equation}
\end{mydef}

The tessellation of the plane with spheres is of course not possible.
There exist three regular, homogeneous - so called Platonic - tessellations: triangles, rectangles, and hexagons.
The isoperimetric quotient of a triangle is $\frac{\pi\sqrt{3}}{9} \approx 0.6046$, for a sphere it's $\frac{\pi}{4} \approx 0.7854$ and for a hexagon $\frac{\pi\sqrt{3}}{6} \approx 0.9069$.

In the classical case the isoperimetric quotient lies between 0 and 1, where the circle itself reaches the maximal value of 1.
For lattice graphs, area and perimeter of segments are an approximation to the classical geometric definitions.
Here the isoperimetric quotient can be bigger than 1, as boundaries have a finite thickness in contrast to spaces which allow infinitesimal thin boundaries.

The isoperimetric quotient of a partition is defined as the weighted mean of the isoperimetric quotient of its segments \cite{schick2012compact}:
\begin{mydef}
	The {\bf isoperimetric quotient} of a partition $\dwp$ is defined as
	\begin{equation}
		\textbf{IPQ}(\dwp) := \frac{1}{|\dwp|} \sum_{S \in \dwp}{|S|\,\textbf{IPQ}(S)}
	\end{equation}
\end{mydef}

\subsubsection{Compression Error}

A good superpixel partition creates segments which represent individual elements well.
For example, the compression quality of a superpixel partition can be measured by comparing segment mean values against individual pixel values.
Here the root mean square error is used to compare the original image against the partitioned image where each pixel is assigned the value of the corresponding superpixel.

\begin{mydef}
	\label{def:dasp_sp_props_ce}
	Let $f : \dwu \rightarrow \dwf$ be a feature annotation into a feature space $\dwf$ equipped with a metric $\|\cdot\|_\dwf$.
	Let $\dwfp_S$ be the selected segment feature for each segment $S \in \dwp$ in the partition.
	The {\bf compression error (CE)} is defined as 
	\begin{equation}
		\textbf{CE}(\dwp,f,(\dwfp_S)_{S \in \dwp}) := \sqrt{\frac{1}{|\dwp|}
		\sum_{S \in \dwp}{\sum_{x \in S}\|{f(x) - \dwfp_S\|^2_\dwf}}}
	\end{equation}
\end{mydef}

The smaller the compression error, the better a superpixel segmentation is able to represent the original image data.
It is limited by the feature variation in the image and stands in direct competition to superpixel compactness.
% It should go towards 0 for an increasing number of superpixels.


\subsubsection{Explained Variation}
\label{sec:apx_metric_sp_ev}

Another metric for the compression quality of a superpixel partition is the explained variation metric.

\begin{mydef}
	\label{def:dasp_sp_props_ev}
	Let $f : V \rightarrow \dwf$ be a feature annotation into a feature space $\dwf$ equipped with a metric $\|\cdot\|_\dwf$ and a mechanism to compute mean values.
	Let $\dwfp_S$ be selected segment features for each segment $S \in \dwp$ in the partition and $\overline{\dwfp} := \text{mean}_{x \in \dwp}{f(x)}$ the mean feature over the whole partition.
	The {\bf explained variation} of the selected features $(\dwfp_S)_{S \in \dwp}$ is defined as 
	\begin{equation}
		\textbf{EV}(\dwp,f,(\dwfp_S)_{S \in \dwp}) := \frac{\sum_{S \in \dwp}{|S|\|\dwfp_S-\overline{\dwfp}\|^2_\dwf}}{\sum_{x \in \dwp}{\|f(x)-\overline{\dwfp}\|^2_\dwf}}
	\end{equation}
\end{mydef}

The better segment features represent all pixels in the segment, the smaller the explained variation measure.
The metric may have little information content and yield values near to 1 if the overall mean value lies to far away from the majority of individual feature values.
For example if pixel feature values would be 3D positions on a sphere, the overall mean would be the centre of the sphere.
Then the local distance between pixel features and segment features may be much smaller than the distance between segment features and the overall mean feature.

\subsubsection{Uniform distribution}

Another desired property of superpixels is a uniform distribution over the given supporting structure.
In the following we build a segment density function over a graph by placing Gaussian kernels at the centre of each segment.
This density function is then compared against the uniform distribution over the vertices.

\begin{mydef}
	Let $Q \subset \dwu$ be a finite set of points and $k$ kernel function.
	The {\bf density} with respect to $Q$ is defined as
	\begin{equation}
		\rho_{Q}(v) := \sum_{q \in Q}{k(\|v-q\|)} \,.
	\end{equation}
\end{mydef}

A typical two-dimensional density kernel is given by the multivariate normal distribution
\begin{equation}
	k_{\sigma}(x) := \frac{1}{2\pi\sigma^2} \, e^{-\frac{1}{2}\frac{x^2}{\sigma^2}}
\end{equation}
The optimal uniform distribution would result in a constant density of $\rho_0 := \frac{|Q|}{|\dwu|}$ for all vertices.
Assuming that $G$ is a lattice graph, the optimal kernel parameter $\sigma^{*}$ is chosen such that $k_{\sigma^{*}}(0) = \rho_0$.
This gives $\sigma^{*} = \sqrt{\frac{|V|}{2 \pi |\dwu|}}$ for the standard deviation of the Gaussian kernel.

% For a graph which has only
% \begin{mydef}
% 	The {\bf centre} of a segment $S \in \dwp$ is defined as
% 	\begin{equation}
% 		\text{centre}(U) := \argmin_{c \in U}{\sum_{x \in U}{L_{min}(c,x)}}
% 	\end{equation}
% \end{mydef}
% The minimum of the above equation may be reached by several vertices in $U$.
% In this case any of the minimal vertices is picked.

% For a lattice graph, this defintion of the centre uses the Manhatten distance which does not result in the Euclidean centre of a set of vertices when embedded into the Euclidean space.

The centres of segments of a partition define a density over the graph.
The centre of a segment is simply the mean position of all points in the segment.
The closer this density is to the constant density, the more uniform segments are distributed.
\begin{mydef}
	Let $C$ be the set of centre points of the partition $\dwp$.
	The {\bf uniform distribution error (UDE)} of a partition is defined as
	\begin{equation}
		\textbf{UDE}(P) := \sqrt{\sum_{v \in \dwp}{\left(\rho_{C}(v) - \frac{|C|}{|\dwp|}\right)^2}}
	\end{equation}
\end{mydef}



\section{Quality metrics for superpoints}
\label{sec:apx_metric_dasp}


To measure the quality of 3D superpixels some additional superpixel properties are introduced.
The 3D isoperimetric quotient, which is similar to the normal isoperimetric quotient defined in image space, measures superpixel area and perimeter in 3D.
The 3D isoperimetric quotient for a segment is computed as in \refdef{dasp_sp_props_ipq}:
\begin{equation}
	\textbf{IPQ}_\text{3D}(S) := \frac{4 \pi \, \textbf{Area}_\text{3D}(S)}{\textbf{Perimeter}_\text{3D}(S)^2}
	\label{eq:dasp_ipq3}
\end{equation}
with the 3D pixel area computes as
\begin{equation}
	\textbf{Area}_\text{3D}(S) := \frac{1}{f^2} \sum_{u \in S}{\frac{D(u)^2}{n(u)_z}} \
\end{equation}
and the 3D pixel boundary computed as
\begin{equation}
	\textbf{Perimeter}_\text{3D}(S) := \frac{1}{f} \sum_{u \in \partial S}{\frac{D(u)}{n(u)_z}} \,.
\end{equation}
with $f$ the camera focal length, $D(u)$ point depth and $n(u)$ point normal.
Using the $z$ component of the point normal one can approximate surface projection distortion, as seen in \refeq{dasp_normal} and \refeq{dasp_point_area}.



% \section{Quality metrics for video stream segmentations}
% \label{sec:apx_metric_tsp}


% Xu et al. \cite{xu2012evaluation} gives an overview of quality measures for video segmentations and evaluates several methods based on theses metrics.
% Metrics for video segmentation are mostly a direct extension of metrics used in image segmentation which are presented in \refsec{apx_metric_sp}.
% Instead of the two-dimensional pixel lattice graph $\dwgi$ the spatio-temporal three-dimensional lattice graph $\dwgv$ is used.


% \subsubsection{Undersegmentation Error (tempo-spatial)}

% \begin{mydef}
% Let $\dwp$ be a partition of a video $\dwgv$ and $\dwpb$ a ground truth partition used as a reference.
% The {\bf 3D Undersegmentation Error} \index{undersegmentation error (3D)} is defined similar to the 2D case (\refdef{dasp_sp_props_use}) as 
% \begin{equation}
% \label{def:tdasp_use3}
% 	\textbf{USE}^{\textbf{3D}}_{\gamma}(\dwp|\dwpb) :=
% 		\frac{1}{\vol(\dwp)}\,
% 		\sum_{T \in \dwpb}{
% 			\sum_{S \in \dwp}{
% 				\,\vol(S) \, 1_\mathbb{B}\left[\frac{\vol(S \cap T)}{\vol(S)} > \gamma\right]
% 			}}
% 		 - 1
% \end{equation}
% \end{mydef}


% \subsubsection{Boundary Recall (tempo-spatial)}

% \begin{mydef}
% \label{def:tdasp_br3}
% 	Let $\dwp$ be a partition of a video $\dwgv$ and $\dwpb$ a ground truth partition used as a reference.
% 	The {\bf 3D boundary recall} \index{boundary recall (3D)} with respect to $\dwp$ and a maximum reach $\delta \in \realspos$ is defined similar to the 2D case (\refdef{dasp_sp_props_br}) as
% 	\begin{equation}
% 		\textbf{BR}^{\textbf{3D}}_{\delta}(\dwp|\dwpb) := \frac{1}{\vol(\partial\,\dwp)} \sum_{v \in \partial\,\dwp}{1_\mathbb{B}\left[\min_{x \in \partial \dwpb}{\|v-x\|} < \delta\right]}
% 	\end{equation}
% \end{mydef}


% \subsubsection{Explained Variation (tempo-spatial)}

% \begin{mydef}
% 	\label{def:tdasp_ev3}
% 	Let $\dwv : \dwgv \rightarrow \dwf$ be a video sequence and $\dwp$ a partition of the underlying graph $\dwgv$.
% 	Let $\dwf$ be equipped with a metric ${\|\cdot\|}_{\dwf}$ and a mechanism to compute mean values.
% 	Let $\dwfs(S)$ be selected segment features for each segment $S \in \dwp$ in the partition and $\overline{\dwfp} := \text{mean}_{x \in \dwp}{\dwv(x)}$ the mean feature over the whole partition.
% 	The {\bf explained variation} of the selected features $\dwfs$ is defined similar to the 2D case (\refdef{dasp_sp_props_ev}) as 
% 	\begin{equation}
% 		\textbf{EV}^{\textbf{3D}}(\dwp,\dwfs) := \frac{\sum_{S \in \dwp}{\vol(S)\,\|\dwfs(S)-\overline{\dwfp}\|^2_\dwf}}{\sum_{x\in\dwp}{\|V(x)-\overline{\dwfp}\|^2_\dwf}}
% 	\end{equation}
% \end{mydef}




\chapter{Supplementary Results}
\label{sec:apx_results}



\section{\dasp}
\label{sec:apx_results_sp}

The following figures show more evaluation results for the \dasp algorithm from section \refsec{dasp}.

\begin{itemize}
	\item \Reffig{dasp_super_eval_db_1} and \reffig{dasp_super_eval_db_1} show superpixels for a set of input colour images (depth images are not shown)
	\item \Reffig{dasp_eval_br_use} compares \daspK against reference methods using the boundary recall and undersegmentation error metrics.
	In these and the following figures the diagrams in the left column display the measure for a varying number of superpixels and a the number of iterations for \dalicK fixed to 5.
	The diagrams in the right column display the measure for a varying number of \dalicK iterations and a fixed number of superpixels of 1000.
	Values are the mean over the whole dataset.
	\item \Reffig{dasp_eval_ipq}: evaluation of isoperimetric quotient
	\item \Reffig{dasp_eval_ew}: evaluation of properties from eigenvalues
	\item \Reffig{dasp_eval_ev}: evaluation of explained variation
	\item \Reffig{dasp_eval_ce}: evaluation of compression error
\end{itemize}

\begin{figure}[!t]
	\centering
	\daspevalimgrow{001}\\
	\daspevalimgrow{003}\\
	\daspevalimgrow{004}\\
	\daspevalimgrow{007}
	\caption{
		Examples for \dasp}
	\label{fig:dasp_super_eval_db_1}
\end{figure}

\begin{figure}[!t]
	\centering
	\daspevalimgrow{008}\\
	\daspevalimgrow{009}\\
	\daspevalimgrow{010}\\
	\daspevalimgrow{002}
	\caption{
		Examples for \dasp (cont.)}
	\label{fig:dasp_super_eval_db_2}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=0.5\textwidth]{dasp/legend.pdf}
	\\
	\includegraphics{dasp/br_num.pdf}
	\includegraphics{dasp/br_its.pdf}
	\includegraphics{dasp/use_num.pdf}
	\includegraphics{dasp/use_its.pdf}
	\caption[\daspK evaluation of boundary recall and undersegmentation error]{
		{\bf Top to bottom}: Boundary recall and undersegmentation error.
		{\bf Left}: Results for varying number of superpixels.
		{\bf Right}: Results for varying number of iterations for \dalicK.}
	\label{fig:dasp_eval_br_use}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=0.5\textwidth]{dasp/legend.pdf}
	\\
	\includegraphics{dasp/ipq_num.pdf}
	\includegraphics{dasp/ipq_its.pdf}
	\includegraphics{dasp/ipq3_num.pdf}
	\includegraphics{dasp/ipq3_its.pdf}
	\caption[\daspK evaluation of isoperimetric quotient]{
		{\bf Top to bottom}: Isoperimetric quotient (2D) and isoperimetric quotient (3D).
		{\bf Left}: Results for varying number of superpixels.
		{\bf Right}: Results for varying number of iterations for \dalicK.}
	\label{fig:dasp_eval_ipq}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=0.5\textwidth]{dasp/legend.pdf}
	\\
	\includegraphics{dasp/ew_thick_num.pdf}
	\includegraphics{dasp/ew_thick_its.pdf}
	\includegraphics{dasp/ew_ecc_num.pdf}
	\includegraphics{dasp/ew_ecc_its.pdf}
	\includegraphics{dasp/ew_flat_num.pdf}
	\includegraphics{dasp/ew_flat_its.pdf}
	\includegraphics{dasp/ew_area_num.pdf}
	\includegraphics{dasp/ew_area_its.pdf}
	\caption[\daspK evaluation of properties from eigenvalues]{
		{\bf Top to bottom}: metrics computed from superpixel eigenvalue analysis: Thickness, Eccentricity, Flatness and Area.
		{\bf Left}: Results for varying number of superpixels.
		{\bf Right}: Results for varying number of iterations for \dalicK.}
	\label{fig:dasp_eval_ew}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=0.5\textwidth]{dasp/legend.pdf}
	\\
	\includegraphics{dasp/ev_c_num.pdf}
	\includegraphics{dasp/ev_c_its.pdf}
	\includegraphics{dasp/ev_d_num.pdf}
	\includegraphics{dasp/ev_d_its.pdf}
	\includegraphics{dasp/ev_v_num.pdf}
	\includegraphics{dasp/ev_v_its.pdf}
	\includegraphics{dasp/ev_n_num.pdf}
	\includegraphics{dasp/ev_n_its.pdf}
	\caption[\daspK evaluation of explained variation]{
		{\bf Top to bottom}: Expected variation for colour, depth and 3D position.
		{\bf Left}: Results for varying number of superpixels.
		{\bf Right}: Results for varying number of iterations for \dalicK.}
	\label{fig:dasp_eval_ev}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=0.5\textwidth]{dasp/legend.pdf}
	\\
	\includegraphics{dasp/ce_c_num.pdf}
	\includegraphics{dasp/ce_c_its.pdf}
	\includegraphics{dasp/ce_d_num.pdf}
	\includegraphics{dasp/ce_d_its.pdf}
	\includegraphics{dasp/ce_v_num.pdf}
	\includegraphics{dasp/ce_v_its.pdf}
	\includegraphics{dasp/ce_n_num.pdf}
	\includegraphics{dasp/ce_n_its.pdf}
	\caption[\daspK evaluation of compression error]{
		{\bf Top to bottom}: Compression error for colour, depth, 3D position and 3D normal.
		{\bf Left}: Results for varying number of superpixels.
		{\bf Right}: Results for varying number of iterations for \dalicK.}
	\label{fig:dasp_eval_ce}
\end{figure}



\clearpage

\section{\ebpf}

The following figures show more results for the \ebpf algorithm from section \refsec{ebv}.

\begin{itemize}
\item \Reffig{ebv_eval_sim} shows a comparison of tracked path an actual path in the simulated robot self-localization scenario from \refsec{ebv_loc}
\item \Reffig{ebv_eval_gt} shows a comparison of tracked path an ground truth in the experimental robot self-localization scenario from \refsec{ebv_loc}
\end{itemize}

\clearpage

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics{ebv/eval/scn_s01}
	\includegraphics{ebv/eval/scn_s02}
	\includegraphics{ebv/eval/scn_s03}
	\includegraphics{ebv/eval/scn_s04}
	\caption[\ebpfK tracking results (simulation)]{
		Tracking results from simulation for four scenarios.
		Depicted are raw tracking results (black), smoothed tracking results using a mean filter (blue) and ground truth (red).
		Axes units are in meters.
	}
	\label{fig:ebv_eval_sim}
\end{figure}

\begin{figure}
	\centering
	\fwn{2}
	\includegraphics{ebv/eval/scn_10}
	\includegraphics{ebv/eval/scn_11}
	\includegraphics{ebv/eval/scn_12}
	\includegraphics{ebv/eval/scn_13}
	\caption[\ebpfK tracking results (ground truth)]{
		Experimental tracking results for four scenarios compared to ground truth from an overhead tracking system. Depicted are raw tracking results (black), smoothed tracking results using a mean filter (blue) and ground truth (red).
		Axes units are in meters.
	}
	\label{fig:ebv_eval_gt}
\end{figure}



\clearpage

\section{\ebslam}

The following figures show more results for the \ebslam algorithm from section \refsec{ebslam}.

\begin{itemize}
\item \Reffig{ebslam_eval_trajectory_1} and \reffig{ebslam_eval_trajectory_2} compare the tracked path against ground truth
\item \Reffig{apx_ebslam_ts1} and \reffig{apx_ebslam_ts2} show the development of path and map over time.
\end{itemize}

\begin{figure}
	\centering
	\fwn{3}
	\includegraphics{ebslam/results/05_path_map.png}
	\includegraphics{ebslam/results/05_path_plot.pdf}
	\includegraphics{ebslam/results/05_errors_plot.pdf}
	\includegraphics{ebslam/results/17_path_map.png}
	\includegraphics{ebslam/results/17_path_plot.pdf}
	\includegraphics{ebslam/results/17_errors_plot.pdf}
	\includegraphics{ebslam/results/30_path_map.png}
	\includegraphics{ebslam/results/30_path_plot.pdf}
	\includegraphics{ebslam/results/30_errors_plot.pdf}
	\includegraphics{ebslam/results/31_path_map.png}
	\includegraphics{ebslam/results/31_path_plot.pdf}
	\includegraphics{ebslam/results/31_errors_plot.pdf}
	\caption[\ebslamoK results]{
		{\bf Top to bottom:} Three examples out of a total of 40 from the dataset.
		{\bf Left:} Map and path as created by our method.
		{\bf Middle:} Trajectories resulting from our method (red) and the external tracking system (blue).
		The trajectory starting point is marked with X.
		{\bf Right:} Positional and rotational error over event time.}
	\label{fig:ebslam_eval_trajectory_1}
\end{figure}

\begin{figure}
	\centering
	\fwn{3}
	\includegraphics{ebslam/results/11_path_map.png}
	\includegraphics{ebslam/results/11_path_plot.pdf}
	\includegraphics{ebslam/results/11_errors_plot.pdf}
	\includegraphics{ebslam/results/23_path_map.png}
	\includegraphics{ebslam/results/23_path_plot.pdf}
	\includegraphics{ebslam/results/23_errors_plot.pdf}
	\includegraphics{ebslam/results/evaluation_examples_40_map.jpg}
	\includegraphics{ebslam/results/evaluation_examples_40_combined.pdf}
	\includegraphics{ebslam/results/evaluation_examples_40_errors.pdf}
	\includegraphics{ebslam/results/evaluation_examples_21_map.jpg}
	\includegraphics{ebslam/results/evaluation_examples_21_combined.pdf}
	\includegraphics{ebslam/results/evaluation_examples_21_errors.pdf}
	\caption[\ebslamoK results (cont.)]{
		{\bf Top to bottom:} Three more examples out of a total of 40 from the dataset.
		{\bf Left:} Map and path as created by our method.
		{\bf Middle:} Trajectories resulting from our method (red) and the external tracking system (blue).
		The trajectory starting point is marked with a X.
		{\bf Right:} Positional and rotational error over event time.}
	\label{fig:ebslam_eval_trajectory_2}
\end{figure}

\begin{figure}
	\centering
	\fwn{4}
	\includegraphics{ebslam/slam_time_series1/img-0.jpg}
	\includegraphics{ebslam/slam_time_series1/img-1.jpg}
	\includegraphics{ebslam/slam_time_series1/img-2.jpg}
	\includegraphics{ebslam/slam_time_series1/img-3.jpg}
	\includegraphics{ebslam/slam_time_series1/img-4.jpg}
	\includegraphics{ebslam/slam_time_series1/img-5.jpg}
	\includegraphics{ebslam/slam_time_series1/img-6.jpg}
	\includegraphics{ebslam/slam_time_series1/img-7.jpg}
	\caption[Map and path generation over time (example 1)]{
		Example for map and path generation over time.
		Displayed is a time series of map and path at fixed time intervals.
		In this scenario the robot moved on its own while exploring the environment.
		The wiggly parts of the trajectory indicate that the robot hit an environment obstacle on the ground.
	}
	\label{fig:apx_ebslam_ts1}
\end{figure}

\begin{figure}
	\centering
	\fwn{4}
	\includegraphics{ebslam/slam_time_series2/img-0.jpg}
	\includegraphics{ebslam/slam_time_series2/img-1.jpg}
	\includegraphics{ebslam/slam_time_series2/img-2.jpg}
	\includegraphics{ebslam/slam_time_series2/img-3.jpg}
	\includegraphics{ebslam/slam_time_series2/img-4.jpg}
	\includegraphics{ebslam/slam_time_series2/img-5.jpg}
	\includegraphics{ebslam/slam_time_series2/img-6.jpg}
	\includegraphics{ebslam/slam_time_series2/img-7.jpg}
	\caption[Map and path generation over time (example 2)]{
		A second example like in \reffig{apx_ebslam_ts1}.
	}
	\label{fig:apx_ebslam_ts2}
\end{figure}


\clearpage

\section{\ebslame}

The following figures show more results for the \ebslame algorithm from section \refsec{eb3}.

\begin{itemize}
\item \Reffig{apx_eb3_paths} compares tracked paths against ground truth
\item \Reffig{apx_eb3_sweeps_1} shows parameter sweeps and results for the positional root-mean-square error and the runtime performance
\item \Reffig{apx_eb3_sweeps_2} shows more parameter sweeps and results for the positional root-mean-square error and the runtime performance
\end{itemize}


\begin{figure}
	\centering
	\fwn{2}
	\includegraphics[width=7cm]{eb3/paths/scn1_take01.png}%
	\includegraphics[width=7cm]{eb3/paths/scn2c_take03.png}
	\includegraphics[width=7cm]{eb3/paths/scn2c_take04.png}%
	\includegraphics[width=7cm]{eb3/paths/scn2d_take04.png}
	\includegraphics[width=7cm]{eb3/paths/scn3_take01.png}%
	\includegraphics[width=7cm]{eb3/paths/scn3_take03.png}
	\caption[Trajectories from \ebslame against ground truth]{
		6 examples for trajectories tracked with \ebslame (red) compared against grount truth (blue) from an overhead tracking system.
	}
	\label{fig:apx_eb3_paths}
\end{figure}

\begin{figure}
	\centering
	\fwnfull{}
	\includegraphics{eb3/sweeps/batchSize.pdf}\\
	\includegraphics{eb3/sweeps/eventCut.pdf}\\
	\includegraphics{eb3/sweeps/mapCellSize.pdf}
	\caption[RMSE and runtime for \ebslameK (1)]{
		RMSE and execution time relative to take duration plotted for the following parameters:
		Mini-batch size $B$ (top), percentage of processed events (middle) and voxel size (bottom). 
	}
	\label{fig:apx_eb3_sweeps_1}
\end{figure}

\begin{figure}
	\centering
	\fwnfull{}
	\includegraphics{eb3/sweeps/diffusePos.pdf}\\
	\includegraphics{eb3/sweeps/diffuseRot.pdf}
	\caption[RMSE and runtime for \ebslameK (2)]{
		RMSE and execution time relative to take duration plotted for the following parameters:
		Standard deviation for position diffusion (top) and angular standard deviation for rotation diffusion (bottom). 
	}
	\label{fig:apx_eb3_sweeps_2}
\end{figure}


% \chapter{Software}
% \label{sec:apx_software}



% \section{libpds, libasp, libdasp and dasp\_gui}



% \section{libtdasp and tdasp\_gui}



% \section{libedvs, libedvstools, libedvsslam and edvs\_slam\_gui}


% For this thesis a software landscape (see \reffig{edvs_programs}) has been developed which is capable of performing the required tasks for event-based tracking.
% Most of the software was developed by the author with the language C++ using Qt \cite{nokia2012} for GUI applications and boost for various tasks.
% Some tasks involves usage of auxiliary 3rd party software.

% \begin{description}
% 	\item[EdvsSimulatePath] generates a path by using spline interpolation with user defined control points. Examples of generated paths are: line, circle, sinus and eight.
% 	\item[EdvsSimulateMap] generates a simulated line map consisting of randomly placed lines and circles.
% 	\item[EdvsSimulateEvents] simulated the eDVS sensor using a path and a map to generate events and store them in a compressed file.
% 	\item[EdvsCapture] encapsulates the USB/COM interface to capture events from the eDVS retina sensor.
% 	\item[Ground Truth Capture \& Calibration:] Capturing videos of the robot with a marker, capturing ground truth data from video with ARToolKit \cite{kato2000} and calibration using Mathematica \cite{web:mathematica}.
% 	\item[Map Stitching:] Photos from the ceiling map were stitched together to form a calibrated, well aligned and normalized map using the free panorama stitching software Hugin \cite{Source}.
% 	\item[EdvsEventViewer] is a C++/Qt program to visualize events.
% 	\item[EdvsTracking] performs event-based self-localization using a ceiling map and the recorded or simulated retina events. This is the main tracking program used to create the evaluation results.
% 	\item[Evaluation:] Mainly Mathematica was used to compare trajectories and evaluate parameter selection.
% \end{description}

% \begin{figure}[h!]
% 	\centering
% 	\includegraphics[width=\textwidth]{ebv/edvs_programs}
% 	\caption{Edvs software landscape: Depicted are C++ programs (yellow), data formats (blue) and usage of auxiliary programs (red).}
% 	\label{fig:edvs_programs}
% \end{figure}




% \chapter{Datasets}
% \label{sec:apx_datasets}


% \subsubsection{RGB-D segmentation dataset}

% \subsubsection{RGB-D video dataset}

% %\subsubsection{Pancake dataset}

% %\subsubsection{Sandwich dataset}

% %\subsubsection{Navigation/avoidance dataset}

% \subsubsection{eDVS SLAM dataset}

% \subsubsection{eDVS Exploration dataset}

% \subsubsection{eDVS 3D dataset}
