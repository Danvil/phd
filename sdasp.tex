%!TEX root = phd.tex



\chapter{S-DASP Image Segmentation}
\label{sec:sdasp}


\sdaspK is a novel segmentation technique for RGB-D images which generates excellent segments by using both colour and depth information.
\sdaspK is highly efficient as it uses the sparse image representation provided by \dasp.

\begin{figure}[h!]
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/teaser/color.png}
	\incgfxfwn{daspseg/teaser/segment_color.png}\\
	\incgfxfwn{daspseg/teaser/segment_contours.png}
	\incgfxfwn{daspseg/teaser/segment_labels.png}
	\caption[\sdaspK overview]{
		{\bf Top}: Input RGB-D image (only colour is shown) and depth-adaptive superpixels.
		{\bf Bottom}: Ultrametric contour graph indicating boundary strength and final segments.}
	\label{fig:sdasp_teaser}
\end{figure}

\clearpage



\section{Introduction to \sdaspK}

As already introduced in \refsec{asp_sp_intro}, image segmentation aims towards dividing an image into a set of segments which shall fulfil two criteria: intra-segment similarity and inter-segment dissimilarity \cite{ren2003}.
In the previous chapter, the oversegmentation technique \dasp for RGB-D images has been introduced which provides a solution to the problem of inter-segment similarity (see \refsec{dasp_intro}).
\daspK divides an image into segments of similar pixels, but different superpixels may be still similar to each other and in general do not satisfy inter-segment dissimilarity.
In this chapter, the novel image segmentation technique \sdasp (\sdaspK) \cite{weikersdorfer2012dasp} for RGB-D images is presented.
\sdaspK combines \daspK superpixels into segments which in addition to inter-region similarity also satisfy inter-segment dissimilarity (see \reffig{sdasp_intro}).

\begin{figure}[b!]
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/intro/color_with_segments.png}
	\incgfxfwn{daspseg/intro/segments_with_border.png}
	\caption[Example for RGB-D image segmentation]{
		{\bf Left}: Colour image of a possible manual ground truth annotation.
		{\bf Right}: \sdaspK image segmentation (colours) using depth-adaptive superpixels (white lines).}
	\label{fig:sdasp_intro}
\end{figure}

Merging superpixels into larger segments is not a straight forward task as local cues alone are often ambiguous.
Global methods try to solve this problem by considering the global ensemble of segments in order to make well-informed decisions about how to merge segments.
An important group of methods for global segmentation is spectral graph theory \cite{cheeger1970lower,fiedler1975property,donath1973lower,alon1986eigenvalues,boppana1987eigenvalues,chunk1997spectral}.
In the past, many methods have been proposed which make use of spectral techniques \cite{arbelaez2011contour,knossow2009inexact,krzeminskiproperties,shi2000ncut,umeyama1988eigendecomposition} for segmentation or matching problems.
However, spectral graph analysis requires the computation of eigenvalues and eigenvectors of a matrix which is slower than quadratic in the number of vertices, thus in the number of pixels when operated on the full pixel lattice graph.
This severely limits the size of images and imposes high computational costs even when using down-sampled images.

\sdaspK tackles this problem by using spectral graph theory on the sparse image representation provided by superpixels through \daspK instead of using the full image directly (see \reffig{sdasp_schematic}).
A simple example demonstrates the impact of using superpixels instead of pixels:
The resolution provided by the PrimeSense RGB-D sensor is 640x480 pixels.
Using 1200 depth-adaptive superpixels yields an excellent approximation to the real image preserving most of its information content (\refsec{dasp_eval}).
However, downsampling this image to 1200 pixels yields an image of size 40x30, loosing most of the image information.
Even using 160x120 pixels, a common image size for image segmentation databases, would still yield 16 times more pixels than superpixels and thus a spectral algorithm would be theoretically several magnitudes slower when assuming a theoretic runtime of $\dwbigo(n^{2.5})$ for sparse eigenvalue computation.

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{daspseg/sdasp/sdasp.pdf}
	\caption[Depth-Adaptive Superpixels Segmentation]{
		{\bf From left to right:} Dense pixel grid, oversegmentation (using false colours), superpixels with superpixel graph and superpixel segments.
	}
	\label{fig:sdasp_schematic}
\end{figure}

The following list presents a short overview over the \sdasp algorithm; details are explained in \refsec{sdasp_sdasp}.
\begin{enumerate}
\item Compute superpixels with the \dasp algorithm.
\item Define a weighted graph structure on superpixels expressing local similarity of neighbouring superpixels.
\item Use spectral graph theory to build an ultrametric contour graph (UCG) over the superpixel graph.
\item Compute a segmentation of the original image with the help of the UCG.
\end{enumerate}

The remainder of this chapter is outlined as follows:
In \refsec{sdasp_spectral} spectral graph theory and several related state-of-the-art methods are introduced.
The \sdasp algorithm is presented in detail in \refsec{sdasp_sdasp} and a thorough evaluation and comparison to state-of-the-art algorithm is reported in \refsec{sdasp_evaluation}.


\section{Spectral graph theory}
\label{sec:sdasp_spectral}

This section gives a short introduction to spectral graph theory:
First some general graph theoretic notations (\refsec{sdasp_spectral_general}) are introduced and then the Laplacian of a graph and its relation to connected components of a graph is presented (\refsec{sdasp_spectral_laplacian}).
Two important state-of-the-art segmentation techniques using spectral graph theory are presented at the end of the section in \refsec{sdasp_spectral_ncuts} and \refsec{sdasp_spectral_malik}.


\subsection{General notations for graphs}
\label{sec:sdasp_spectral_general}

In the following  $G=(V,E)$ will denote an undirected, weighted {\bf graph}.
Here $V$ are the graph {\bf vertices}, $n := |V|$ the number of vertices, and $E$ the set of undirected and weighted {\bf edges}.
In the following we assume that edges are not directed, edge weights are always greater than zero, two vertices are at most connected by one edge and that no vertices is connected to itself.
This includes the cases of a regular graphs, like the image pixel lattice graph, or a more general graph resulting for example from a superpixel segmentation by connecting neighbouring superpixels.

% As pixel grid lattice graphs are not only defined by their structure, but also by feature vectors defined over the set of nodes we more generally consider annotated graphs $G=(V,E,\alpha)$ where $\alpha: V \rightarrow F$ an annotation function which assigns an element of a feature space to each graph vertex.
% The most common case for features are RGB colour values, $F = [0,1]^3$, or intensity values, $V = [0,1]$.
% In the following we will especially consider the case where features are a combination of colour and depth values.
% If the vertex annotation map of a graph is not of interest in the current context it will be omitted.

To indicate that two vertices $v, u \in V$ are {\bf connected} by an edge the notation $v \sim u$ will be used.
The {\bf weight} of an edge $e \in E$ is noted with $w(e)$ and the weight of an edge between two connected vertices $u \sim v \in V$ with $w(u,v) \in \realspos$.
If graph edges are not weighted explicitly, edges are assumed to have an equal weight of 1.
The weight of edges connected to vertex is called the {\bf degree} of the vertex.

The edge connectivity can also be expressed by using an {\bf adjacency matrix} $W \in \realspos^{n \times n}$, where $W_{ij}$ is the weight of the edge between nodes $i$ and $j$ or $0$ if theses nodes are not connected.
The adjacency matrix is symmetric as the graph is assumed to be undirected.

A sequence of vertices $v_1 \sim \cdots \sim v_n$, $v_i \in V$, which are pairwise connected by an edge are called a {\bf path}.
The {\bf length} of a path $p=v_1 \sim \cdots \sim v_n$ is defined as as the sum of weights of all its edges:
\begin{equation}
	len(p) := \sum_{i=1}^{n-1}{w(v_i,v_{i+1})} \,.
\end{equation}
The {\bf length of the shortest path} between two vertices $v, u \in V$ is donated with $L_{min}(v,u)$.
For unweighted graphs, $L_{min}(v,u) + 1$ denotes the number of passed nodes when walking from node $u$ to node $v$.
The length of the shortest path between a vertex $v \in V$ and a subset $U \subset V$ is defined in a straight forward way as $L_{min}(v,U) := \min_{u \in U}{L_{min}(v,u)}$.

The boundary of a segment is the number of vertices which have a neighbour which is not part of the segment.
\begin{mydef}
	Let $G=(V,E)$ be a graph, $\dwp$ a partition of the graph nodes $V$ and $S \in \dwp$ a segment.
	The {\bf boundary} $B_G(S)$ of $S$ is defined as
	\begin{equation}
		B_G(S) := \{ v \in S \,|\, \exists \, q \in V \setminus S: v \sim q \} \,.
	\end{equation}
	\label{eq:sdasp_boundarysegment}
\end{mydef}
The boundary $B_G(S)$ is the subset of vertices of a segment $S$ which are connected to at least one vertex which is not in $S$ itself.
The notion of boundary can be directly extended to partitions.
\begin{mydef}
	Let $\dwp$ be a partition. The {\bf boundary of the partition} is defined as
	\begin{equation}
		B_G(\dwp) := \bigcup_{S \in \dwp}{B_G(S)}
	\end{equation}
	\label{eq:sdasp_boundarypartition}
\end{mydef}


\subsection{The Laplacian matrix of a graph}
\label{sec:sdasp_spectral_laplacian}

The Laplacian matrix \cite{chunk1997spectral} is a construct in graph theory which gives information about the structure of a graph.
We will see in the following, that the number of eigenvalues of the Laplacian matrix $L(G)$ of a graph $G$ which are 0 is equal to the number of connected components of $G$.
As seen in the following sections, these results can be generalized to compute optimal cuts for a connected graph, thus making the Laplacian a valuable tool in graph segmentation.

\begin{mydef}
	The {\bf degree matrix} $D(G) \in \realspos^{n \times n}$ of the graph $G$ with adjacency matrix $W \in \realspos^{n \times n}$ is the diagonal matrix defined as
	\begin{equation}
		\big(D(G)\big)_{ii} := \text{deg}(v_i) := \sum_{j=1}^{n}{W_{ij}} \,.
		\label{eq:sdasp_D}
	\end{equation}
\end{mydef}

\begin{mydef}
	The {\bf Laplacian matrix} $L(G) \in \mathbb{R}^{n \times n}$ of the graph $G$ with adjacency matrix $W \in \realspos^{n \times n}$ is defined as
	\begin{equation}
		L(G) := D(G) - W \,.
		\label{eq:sdasp_L}
	\end{equation}
\end{mydef}

\Reffig{dasp_laplacian_example} shows an example of a simple graph and its Laplacian matrix. All edge weights are 1 and the numbers indicate node labels and thus row/colum index.
\begin{figure}[h!]
	\centering
	\begin{minipage}{0.4\columnwidth}
		\includegraphics[width=\textwidth]{extern/6n-graf}
	\end{minipage}
	~~~
	\begin{minipage}{0.4\columnwidth}
	\begin{equation*}
		\left(\begin{array}{rrrrrr}
			 2 & -1 &  0 &  0 & -1 &  0\\
			-1 &  3 & -1 &  0 & -1 &  0\\
			 0 & -1 &  2 & -1 &  0 &  0\\
			 0 &  0 & -1 &  3 & -1 & -1\\
			-1 & -1 &  0 & -1 &  3 &  0\\
			 0 &  0 &  0 & -1 &  0 &  1\\
		\end{array}\right)
	\end{equation*}
	\end{minipage}
	\caption[A simple graph and its Laplacian]{A simple graph \cite{imgSdaspLaplacian} and its Laplacian matrix.}
	\label{fig:dasp_laplacian_example}
\end{figure}

\begin{mylem}
	The Laplacian matrix $L(G)$ is positive-semidefinite.
\end{mylem}
\begin{proof}
	Consider an edge $e=(e_1,e_2) \in E$ of $G$ with weight $w_e := W_{e_1 e_2} > 0$. We define the edge Laplacian matrix
	\begin{equation*}
		\big(L(e)\big)_{ij} := \left\{\begin{array}{l l}
			+w_e & \text{if $i=j=e_1$ or $i=j=e_2$}\\
			-w_e & \text{if $i=e_1$ and $j=e_2$ or $i=e_2$ and $j=e_1$}\\
			0 & \text{otherwise}\\
		\end{array}\right.
	\end{equation*}
	The edge Laplacian matrix is positive semi-definite as
	\begin{equation*}
		\forall\, x \in \mathbb{R}^n: x^T L(e) \, x = w_e \, (x_{e_1} - x_{e_2})^2 \ge 0
	\end{equation*}	
	The Laplacian matrix $L(G)$ can be written as the sum of it's edge Laplacian matrices: $L(G) = \sum_{e \in E}{L(e)}$.
	Thus we have
	\begin{equation}
		\label{eq:dasp_edge_laplacian_psd}
		x^T L(G) \, x = \sum_{e \in E}{x^T L(e) \, x} = \sum_{e \in E}{w_e \, (x_{e_1} - x_{e_2})^2} \ge 0
	\end{equation}
\end{proof}

As $L(G)$ is symmetric and positive-semidefinite, all its eigenvalues are real and positive or zero.

\begin{mylem}
\label{misc:sdasp_smallest_eigenvalue}
	The smallest eigenvalue of the Laplacian matrix $L(G)$ is always $0$. The corresponding eigenvector is $(1,\ldots,1)^T \in \mathbb{R}^n$.
\end{mylem}
\begin{proof}
	$W \, (1,\ldots,1)^T = (\sum_{j=1}^n{W_{ij}})_i$, thus $L(G) \, (1,\ldots,1)^T = 0$, and thus $(1,\ldots,1)^T$ is an eigenvector of $L(G)$ with eigenvalue 0.
\end{proof}

\begin{myprop}
	The Laplacian matrix $L(G)$ has an eigenvalue 0 for each connected component of $G$.
\end{myprop}
\begin{proof}
	Let $I \ne \emptyset$ be the set of indices of the nodes of one of the connected components.
	Define $v \in \mathbb{R}^n$ as
	\begin{equation*}
		v_i := \left\{\begin{array}{l l}
			1 & i \in I\\
			0 & i \not\in I\\
		\end{array}\right.
	\end{equation*}
	We have $L(G) \, v = 0$ for the same reason as in the proof of lemma \ref{misc:sdasp_smallest_eigenvalue}, as edges with weight greater than 0 are only formed between vertices of the conneted component.
	Thus $v$ is an eigenvector with eigenvalue 0.
	Eigenvectors constructed in this way are pair-wise orthogonal, thus follows the assertion.
\end{proof}

\begin{mylem}
\label{eq:dasp_component_count}
	If the graph $G$ is connected, only the smallest eigenvalue is 0.
\end{mylem}
\begin{proof}
	Let $x \in \mathbb{R}^n$ such that $L(G) \, x = 0$.
	By \refeq{dasp_edge_laplacian_psd} we have
	\begin{equation*}
		0 = x^T L(G) \, x = \sum_{e \in E}{w_e \, (x_{e_1} - x_{e_2})^2}\,,
	\end{equation*}
	which implies $x_{e_1} = x_{e_2}$ for all $e = (e_1,e_2) \in E$. As the graph $G$ is connected, there is a path from each vertex to each other vertex, thus all values of $x$ must be equal. This implies that x is a multiple of the eigenvector $(1,\ldots,1)^T \in \mathbb{R}^n$. 
\end{proof}

For a connected graph, the second smallest eigenvalue $\lambda_2$ is called the {\bf Fiedler vector} \cite{fiedler1975property} and gives interesting information about how well a graph can be cut into two segments in terms of minimizing the weight of cut edges.

\begin{mydef}
	Let $A \cap B = V$ a partition of the graph $G = (V,E)$.
	The {\bf cut} of a the partition is defined as
	\begin{equation}
		\text{cut}(A,B) := \sum_{a \in A, b \in B}{w(a,b)}
	\end{equation}
\end{mydef}
The cut of a two-element partition represents the total weight of edges which have to be cut to disconnect the two segments.
It is 0 if the segments are disconnected and small for segments which are only connected by few edges with low weights.

The minimal possible cut can be estimated using the Fiedler vector:
\begin{myprop}[Cheeger's Inequality]
	Let $W_{ij} \in \{0,1\}$ and $d$ be the maximial degree of all vertices.
	Define
	\begin{equation*}
		h_{*} := \min_{A \cap B \subset V} \frac{\text{cut}(A,B)}{\min(|A|,|B|)}\,,
	\end{equation*}
	then
	\begin{equation}
		\frac{h_{*}^2}{2 d} \le \lambda_2 \le h_{*}\,.
	\end{equation}
\end{myprop}
\begin{proof}
	See \cite{cheeger1970lower,chunk1997spectral,krzeminskiproperties}.
\end{proof}

Thus the Fielder vector indicates how well a graph can be partitioned into two segments in the means of cutting as few edges as possible.
These results can be extended to a better behaving cut measure resulting in the normalized cuts algorithm explained in the next section.


\subsection{The normalized cuts algorithm}
\label{sec:sdasp_spectral_ncuts}

The normalized cuts algorithm by Shi et al. \cite{shi2000ncut} presents a new measure for the cut of a graph and provides an algorithm for finding the minimal cut under this measure based on the Fiedler vector of a generalized eigenvalue problem.
In contrast to the classical cut measure \cite{wu1993optimal}, the normalized cut measure is normalized with respect to the total possible connectivity of a subset of the graph vertices to itself.
This has the advantage that it does not extensively prefer to cut of small segments.

The normalized cut measure of a partition $A \cap B = V$ is defined as
\begin{equation}
\label{eq:sdasp_ncut}
	\text{Ncut}(A) := \frac{\text{cut}(A,B)}{\text{cut}(A,V)} + \frac{\text{cut}(A,B)}{\text{cut}(B,V)} \,.
\end{equation}
Finding the minimal cut is an NP-hard problem, but approximate solutions can be found by embedding the problem in the domain of real numbers.
Representing a subset of $V$ by its indicator vector $z \in \{0,1\}^{|V|}$, it can be shown that the minimal normalized cut is found by solving
\begin{equation}
\label{eq:sdasp_ncut_min}
	\min_z\,\text{Ncut}(z) = \min_x \frac{x^T \, L(G)\, x}{x^T \,D(G)\, x}
\end{equation}
under some additional constraints on $x$.
Moreover one can see that the second-smallest eigenvector of the generalized eigenvalue problem 
\begin{equation}
\label{eq:sdasp_ncut_min_approx}
	L(G) \, x = \lambda\,D(G) \, x \,.
\end{equation}
is an approximative solution to \refeq{sdasp_ncut_min}.
For details see \cite{shi2000ncut}.

If $G$ is connected, the degree matrix $D(G)$ has only non-zero, positive entries.
In this case the generalized eigenvalue problem \refeq{sdasp_ncut_min_approx} can be transformed into a standard symmetric eigenvalue problem:
\begin{equation}
\label{eq:sdasp_ncut_min_approx_2}
	D(G)^{-\frac{1}{2}} \, L(G) \, D(G)^{-\frac{1}{2}} \, y = \lambda\, y \,.
\end{equation}
Eigenvectors for the original problem can be computed from the transformed problem with $x = D(G)^{-\frac{1}{2}}\,y$.
For a diagonal matrix $D=(d_{i})_i$ one defines $D^{-\frac{1}{2}}=(d_{i}^{-\frac{1}{2}})_i$.

On can observer that additional eigenvectors to eigenvalues after the second smallest provide further partitions.
The Meila-Shi algorithm \cite{meilpa2001learning} uses this observations to find image segmentations without the need of iterative cutting.


\subsection{Globalization of a local boundary detector}
\label{sec:sdasp_spectral_malik}

Arbela\'ez et al. \cite{arbelaez2011contour} presented a spectral pixel boundary detector sPb for normal colour images which uses spectral graph theory to enhance a local multi-scale boundary detector (mPb) with global image information.
For this method the complete pixel lattice graph is used as the graph $G=(V,E)$, and the weight of an edge connecting two pixels $i,j$ is defined as
\begin{equation}
	W_{ij} := \exp\left(-\frac{1}{\rho}\,\max_{p \in \overline{ij}}{\,\text{mPb}(p)}\right)
\end{equation}
if the distance between pixels $i$ and $j$ is smaller than a fixed radius $R$ and 0 otherwise.
$\overline{ij}$ is the line connecting pixels $i$ and $j$.
$W_{ij}$ defines pixel similarity be separating pixels which lie on opposing sides of a strong boundary.
For this graph the $k$ smallest eigenvalues $\lambda_i$ and eigenvectors $v_i$ of \refeq{sdasp_ncut_min_approx} are computed and the observation is made that eigenvectors carry contour information.
Thus a Gaussian directional gradient filter is applied for varying orientations resulting in the orientation dependent, global boundary detector sPb:
\begin{equation}
	\text{sPb}(p,\theta) := \sum_{i=2}^{k}{\frac{1}{\sqrt{\lambda_i}}{\nabla_\theta \, v_i(p)}} \,.
\end{equation}
Finally a linear combination between local and global boundary detectors is used to compute a globalized boundary propability for each pixel.



\section{Depth-Adaptive Superpixel Segmentation}
\label{sec:sdasp_sdasp}


In the following the \sdasp (\sdaspK) algorithm for RGB-D images is presented in detail.
\sdaspK uses a similar idea as sPb from \refsec{sdasp_spectral_malik} and promotes a local boundary detector to a global detector by using spectral graph theory.
However \sdaspK does not work directly on the dense pixel grid but on a sparse representation -- the set of superpixels provided by \daspK.
This demonstrates how a complex theoretical theory like spectral graph segmentation can be applied in realtime by using an efficient sparse representation like superpixels instead of the dense pixel grid.
To make it possible, some new ideas have to be introduced to transport the idea of globalization from regular to arbitrary graphs.
However it will turn out that this generalization is actually a simplification.
In addition \sdaspK works with RGB-D images which results in a fundamentally better segmentation quality due to the additional consideration of depth information.

\refsec{graphseg_sdasp_graph} describes how a superpixel neighbourhood graph is built on top of \dasp.
In \refsec{graphseg_sdasp_ucg} this neighbourhood graph is transformed into an ultrametric contour graph (UCG) using spectral graph theory.
The UCG is used to compute a graph segmentation and thus the final image segmentation in \refsec{graphseg_sdasp_seg}.


\subsection{Superpixel neighbourhood graph}
\label{sec:graphseg_sdasp_graph}

The starting point of the \sdasp algorithm is an oversegmentation of an RGB-D image into a set of superpixels computed with \daspK.
This superpixel partition $\dwp$ consists of a set of depth-adaptive superpixels, where each superpixel $S \in \dwp$ is annotated with a feature vector $\dwfs \in [{0|1}]^3  \times \sphere{2} \times \mathbb{R}^3$.
The components of the feature vectors are the mean colour $\dwfs_c$, mean position $\dwfs_v$ and mean normal $\dwfs_n$ of the corresponding pixels.

\sdaspK starts by defining a graph structure $G(\dwp) = \left(\dwp, E(\dwp)\right)$ on the set of superpixels by connecting all neighbouring superpixels with edges.
Two superpixels are neighbours, if the superpixel as sets of pixels share a common border on the pixel grid:
\begin{equation}
	(i,j) \in E(\dwp) \text{ iff. } 1 \le i \ne j \le n \wedge \exists\, p \in S_i, q \in S_j : p \sim q \,.
\end{equation}
Due to noisy superpixel boundaries it is beneficial to require that the common boundary has a minimal length:
\begin{equation}
	(i,j) \in E(\dwp) \text{ iff. } 1 \le i \ne j \le n \wedge
		|\{(p,q) \in S_i \times S_j \,|\, p \sim q \}| > \Theta\,.
\end{equation}
In other words, the superpixel neighbourhood graph connects two superpixels $S_i \ne S_j$ if $\text{cut}(S_i,S_j) > \Theta$ with respect to the pixel lattice graph.
The superpixel neighbourhood graph for depth-adaptive superpixels has a well behaving regular almost hexagonal structure due to the uniform distribution of superpixels.
\Reffig{sdasp_graph} shows the superpixel graph for an example image.

\begin{figure}
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/graph/dasp.png}
	\incgfxfwn{daspseg/graph/graph_white.png}
	\caption[Superpixel neighbourhood graph]{
		{\bf Left}: Depth-adaptive superpixels for an RGB-D image.
		{\bf Right}: Neighbourhood graph defined on superpixels.
	}
	\label{fig:sdasp_graph}
\end{figure}

\newcommand{\dwdS}{d_\text{S}}
\newcommand{\dwdSc}[1]{d_{\text{S},#1}}
Superpixel feature values $\dwfs_i$ can be used to further improve the superpixel neighbourhood graph by weighting each edge according to the similarity of the connected superpixels.
Similar superpixels have similar colour, are spatially close and have normals which point in the same direction.
Here a similarity measure on a superpixel partition is defined by using a distance function $\dwdS$ on superpixel features. 
The metric $\dwdS$ is a linear combination of metrics defined over superpixel positions, normals and colour:
\begin{equation}
	\dwdS(\dwfs, \dwfs') := \sum_{\circ=v,n,c}{\lambda_\circ\,\dwdSc{\circ}(\dwfs_\circ, \dwfs_\circ')}
\end{equation}
The distance value is translated into a similarity value using an exponential function:
\begin{equation}
	\dwWs(i,j) := \left\{\begin{array}{l l}
		e^{-\dwdS(\dwfs_i, \dwfs_j)} &\text{ if } (i,j) \in E(\dwp)\\
		0 & \text{otherwise}\\
		\end{array}\right.
	\label{eq:sdasp_graph_sim}
\end{equation}
Edge weights are ranging from 1 for similar superpixels to almost 0 for completely dissimilar superpixels.
\Reffig{sdasp_graph_weighted} shows the weighted superpixel graph for an example image.

\begin{figure}
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/graph/dasp_graph.png}
	\incgfxfwn{daspseg/graph/graph_weight.png}
	\caption[Weighted superpixel neigbhourhood graph]{
		{\bf Left}: Unweighted depth-adaptive superpixel neighbourhood graph.
		{\bf Right}: Depth-adaptive superpixel similarity graph weighted with $\dwWs$ (blue to red to white indicates low to high similarity).}
	\label{fig:sdasp_graph_weighted}
\end{figure}

The individual metrics $\dwdSc{\circ}$ are chosen as follows:
\begin{description}
	\item[Colour superpixel metric]
		Basically, this can be any colour metric.
		For simplicity the Euclidean distance of RGB colour values like in \daspK is chosen.
		\begin{equation}
			\dwdSc{c}(c_1, c_2) := \|c_1 - c_2\|
		\end{equation}
	\item[Position superpixel metric]
		As depth-adaptive superpixels are uniformly distributed in 3D space and have equal radius of $R$, the expected distance between two neighbouring superpixels is $2 R$.
		Thus for spatial distance a metric is used which tolerates the expected distance:
		\begin{equation}
			\dwdSc{v}(v_1, v_2) := \frac{\max(0, \|v_1 - v_2\| - 2R)}{2R}
		\end{equation}
		The Euclidean distance between superpixel centres is additionally normalized by the estimated distance of $2R$ to be independent of the size of superpixels.
	\item[Normal superpixel metric]
		Normal distance could be measured by the angle between the normals.
		However, for three dimensional objects there is a fundamental difference between concave and convex geometry edges.
		While concave edges often represent an object boundary, convex edges almost always do not indicate a segment boundary.
		Thus here a metric is used which only considers the normal distance for superpixels which form a concave edge and yields 0 for superpixels which form a convex edge:
		\begin{equation}
			\dwdSc{n}(n_1,n_2) := \max\left(0, (n_1 - n_2) \circ \frac{v_1 - v_2}{\|v_1 - v_2\|}\right)
		\end{equation}
		Here $v_1$ and $v_2$ are the corresponding 3D point positions.
		% \begin{equation}
		% 	d^{S}_n(n_1, n_2) := \left\{\begin{array}{l l}
		% 		1 - n_1 \circ n_2 & \text{if $(v_1,n_1)$ and $(v_2,n_2)$ form a concave edge}\\
		% 		0 & \text{otherwise}\\
		% 	\end{array}\right.
		% \end{equation}
\end{description}


\subsection{A global segment boundary detector for superpixels}
\label{sec:graphseg_sdasp_ucg}

For the third step of the algorithm, spectral graph theory from \refsec{sdasp_spectral} is used to compute an ultrametric contour graph (UCG) on the superpixel similarity graph.
An UCG is the generalization of an ultrametric contour map (UCM) \cite{arbelaez2006boundary} for non-regular graphs.
This approach is motivated by the boundary detector sPb presented in \refsec{sdasp_spectral_malik} which considers the full pixel lattice graph and improves a local boundary detector by computing eigenvalues of the Laplacian.
However, eigenvalue computation has a runtime of approximately $\dwbigo(n^{2.5})$, thus the performance of sPb is severely impacted by the size of the graph.
The size of the Laplacian is quadratic in the number of image pixels, and even if a sparse matrix representation is used this is still a huge number of elements.
In this section a more elegant boundary computation algorithm is presented which uses a non-regular superpixel graph instead of the dense pixel lattice graph.
This does not change the theoretic, asymptotic runtime behaviour, but a number of vertices which is 1000 times smaller has a huge practical impact.

The three steps of the \sdaspK global segmentation step are as follows:
\begin{enumerate}
\item Compute the graph Laplacian of the superpixel similarity graph
\item Compute $k$ smallest eigenvalues and corresponding eigenvectors
\item Compute the superpixel ultrametric contour graph
\end{enumerate}

The notion ultrametric contour graph is motivated by the concept of an ultrametric contour map.
An UCM is a function into the positive real numbers defined on the pixels of an image which indicates the likelihood that a pixel is part of a segment boundary.
In contrast an ultrametric contour graph is defined on a non-regular graph of an oversegmentation -- here the neighbourhood graph of depth-adaptive superpixels.
Each vertex represents a set of pixels in the image and each edge represents the pixel boundary between the connected superpixels.
The edge weight indicates the likelihood that this pixel boundary piece is part of segment boundary.
Thus an UCG is the sparse analogy of an UCM.
It represents a set of similar boundary pixels, i.e. the boundary of two superpixels, by a single data point, i.e. the edge weight.
An UCG can be converted into an UCM:
for each edge in the graph, the edge weight value is assigned to all segment boundary image pixels represented by the edge (see \reffig{sdasp_graph_ucm}).

\begin{figure}
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/graph/ucm_graph.png}
	\incgfxfwn{daspseg/graph/ucm.png}
	\caption[Superpixel ultrametric contour graph]{
		{\bf Left}: Ultrametric contour graph on a \daspK neighbourhood graph.
		{\bf Right}: Derived ultrametric contour map on the full image.}
	\label{fig:sdasp_graph_ucm}
\end{figure}

Given a connected superpixel graph $G$ with adjacency matrix $W \in \realspos^{n \times n}$, the $k$ smallest eigenvalues $\lambda_t$ of the generalized eigenvalue problem in \refeq{sdasp_ncut_min_approx_2} and corresponding eigenvectors $v_t$ are computed.
The first eigenvalue with value 0 is not used as it does not provide any information as explained in lemma \ref{misc:sdasp_smallest_eigenvalue}.
Each eigenvector assigns a real number to each superpixel.
If the absolute difference of values of adjacent nodes is small this indicates a similar labeling, if it is large a different label is indicated.
The smaller the corresponding eigenvalue, the stronger the similarity.
With this observations, new edge weights on the superpixel graph structure are defined by the adjacency matrix $W^{global}$ as follows:
\begin{equation}
\label{eq:sdasp_Wglobal}
	W^{\text{global}}_{ij} := \left\{\begin{array}{l l}
		\sum_{t=1}^{k}{\frac{1}{\sqrt{\lambda_t}} |v_{ti} - v_{tj}|} & \text{if $(i,j) \in E$} \\
		0 & \text{otherwise} \\
	\end{array}\right.
\end{equation}

For the superpixel graph this much simpler approach using local difference is sufficient as boundary values are not defined \emph{on} graph nodes but \emph{between} nodes on the edges.
Thus $W^{global}$ defines an ultrametric contour graph on the superpixel neighbourhood graph.
This implies a ultrametric contour map on the full image by transferring the boundary strength of an graph edge to all pixels which form the pixel boundary between superpixels connected by the edge.


\subsection{Segmentation of the ultrametric contour graph}
\label{sec:graphseg_sdasp_seg}

\begin{figure}
	\centering
	\fwn{2}
	\incgfxfwn{daspseg/graph/labels_p.png}
	\incgfxfwn{daspseg/graph/labels.png}
	\incgfxfwn{daspseg/graph/labels_m.png}
	\incgfxfwn{daspseg/graph/labels_mm.png}
	\caption[\sdaspK graph labeling]{\sdaspK image segmentations for varying edge cut thresholds.}
	\label{fig:sdasp_graph_labels}
\end{figure}

A segmentation can be performed on an UCG by thresholding edge weights and computing connected components.
This process can also be seen in the general context of the UCM as iteratively increasing the cut threshold and merging one edge after another starting with edges with the lowest weight.
At each step superpixels which are connected by the pruned edge are merged into the same semantic group.
This process creates a continuous transition from the partition where each superpixel is a segment on its own to the partition of one segment which contains all superpixels.
Several steps in such a transition are shown in \reffig{sdasp_graph_labels}.

As weights in the UCG depend on many parameters and especially on the topology of the superpixel neighbourhood graph, it is not easy to derive the optimal cut threshold.
Instead the parameter is used as a free parameter which can be adapted to the requirements of a specific applications.
 % is optimized over a training dataset to maximize precision and recall of segment boundaries relative to manual ground truth annotations.

The segmentation on the UCG graph is directly transformed into a segmentation of the underlying pixel grid by assigning to each pixel the segment ID of its corresponding superpixel.
During this process one of the advantages of using superpixels like \dasp as the underlying elements become apparent: the segmentation on the sparse graph generates pixel-accurate image segments for the full dense pixel grid.
This is not possible if a down-sampled version of the image would have been used.


\section{Evaluation}
\label{sec:sdasp_evaluation}

Precision and recall are well-known measures from information retrieval which are widely used to evaluate the quality of a binary classifier:
\begin{align}
	\text{precision}(A|G) &= \frac{|G \cap A|}{|A|}\\
	\text{recall}(A|G)    &= \frac{|G \cap A|}{|G|}
\end{align}
Here $G$ is the set of ''relevant'' elements which should have been detected and $A$ is the set of ''retrieved'' element which were actually detected by the classifier.
It is important to always consider both quantities together.
A detector which only marks elements as detected if it is very sure can still have a high precision value, and a detector which marks almost everything as detected can have a high recall value.
As precision and recall formulate different goals which in most cases are opposite to each other, the combined measure $F_1$-score is used to measure the overall accuracy of a detector:
\begin{equation}
	F_1 := 2\,\frac{\text{precision} \cdot \text{recall}}{\text{precision} + \text{recall}}
\end{equation}

In the context of image segmentation, segmentations computed by computer algorithms are often compared against manual ground truth annotations from humans.
As it is difficult to provide a measure to identify segments, a common method is to compare the boundary of the computed partition $\dwp$ against the ground truth partition $\dwpb$.
Thus precision, recall and $F_1$ score are defined as
\begin{align}
	\text{precision}(\dwp|\dwpb) &= \frac{|B(\dwp) \cap B(\dwpb)|}{|B(\dwp)|}\\
	\text{recall}(\dwp|\dwpb)    &= \frac{|B(\dwp) \cap B(\dwpb)|}{|B(\dwpb)|}\\
	F_1(\dwp|\dwpb) &= 2\,\frac{|B(\dwp) \cap B(\dwpb)|}{|B(\dwp)| + |B(\dwpb)|}
\end{align}

In the case of \sdasp the quality of computed segmentations is compared against manual ground truth on the supervised dataset from \refsec{dasp}.
The segmentation uses a threshold parameter when concrete segments are computed from the ultrametric contour graph.
The higher the threshold the more segments are merged, the smaller the boundary of the finale partition, and thus the higher is precision.
On the contrary a low threshold leads to more segments and thus to a low precision and a high recall.
Thus the behaviour of \sdaspK is analysed by plotting a curve in a precision/recall coordinate system and computing the maximal possible $F_1$ score.
\Reffig{sdasp_pr} shows results for precision, recall and $F_1$ for three algorithms:
\begin{itemize}
\item \sdaspK: the presented segmentation algorithm which uses \daspK superpixels and makes full use of the depth information.
\item \emph{s-SLIC}: Like \sdaspK but SLIC is used instead of \daspK to compute superpixels. No depth information is used.
\item As a comparison to state-of-the-art algorithms, the dense boundary detector gPb-owt-ucm (see \refsec{sdasp_spectral_malik} and \cite{arbelaez2011contour}) is used. No depth information is used.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.60\textwidth]{daspseg/pr/dasp_segments.pdf}
	\caption[Precision and recall for \sdaspK and state-of-the-art]{Precision/recall for varying cut threshold for \sdaspK, \emph{s-SLIC} and gPb-owt-ucm.}
	\label{fig:sdasp_pr}
\end{figure}

For a fair comparison, all three algorithm have been parametrised to approximately give the same total number of segments for the final segmentation.
Results for \sdaspK and \emph{s-SLIC} are performed with 1000 superpixels and 5 iterations and default values for metric weighting parameters.
gPb-owt-ucm also uses a superpixel technique but does not allow an explicit control over the number of superpixels.

Results demonstrate the superiority of \sdaspK to classical image segmentation algorithms which do not use depth information.
The maximial $F_1$ score of 0.76 is actually already quite near to a typical human $F_1$ score of around 0.8 \cite{arbelaez2011contour}.
Humans do not achieve a perfect $F_1$ score of 1 when compared to each other due to different understandings about the definition of a good segment.

In terms of runtime performance, \sdaspK and the preprocessing step \daspK together require in average 250ms on a standard single core CPU.
This lies in stark contrast to the runtime of gPb-owt-ucm, which was in average 210 seconds - a difference of three magnitudes.

\Reffig{sdasp_results} shows several segmentation results for images from the ground truth data set.
All images have been computed with identical parameters and the optimal cut threshold after \reffig{sdasp_pr} was used.
\sdaspK is capable of merging large planer surfaces into one segment while at the same time preserving small segments with only few superpixels.
It is also apparent how the depth information is used to distinguish ambiguous situations where objects with similar appearance are spatially close to each other.
Sometimes at narrow ridges singular superpixels are split off, which is due to the shortcomings of spectral graph theory.

\begin{figure}[!t]
	\centering
	\fwn{3}
	\incgfxfwn{daspseg/results/004.png}
	\incgfxfwn{daspseg/results/004sp.png}
	\incgfxfwn{daspseg/results/004s.png}
	\incgfxfwn{daspseg/results/008.png}
	\incgfxfwn{daspseg/results/008sp.png}
	\incgfxfwn{daspseg/results/008s.png}
	\incgfxfwn{daspseg/results/009.png}
	\incgfxfwn{daspseg/results/009sp.png}
	\incgfxfwn{daspseg/results/009s.png}
	\incgfxfwn{daspseg/results/011.png}
	\incgfxfwn{daspseg/results/011sp.png}
	\incgfxfwn{daspseg/results/011s.png}
	\caption[Examples for \sdaspK]{
		{\bf Top to bottom:} Four examples for \sdaspK segmentations.
		{\bf Left to right:} Colour image, \daspK superpixels and \sdaspK segments (false colours indicating segments).
	}
	\label{fig:sdasp_results}
\end{figure}
