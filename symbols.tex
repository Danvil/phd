%!TEX root = phd.tex

\newcommand{\asp}{\emph{Adaptive Superpixels}\xspace}
\newcommand{\aspK}{\emph{ASP}\xspace}
\newcommand{\spds}{\emph{Simplified Poisson Disk Sampling}\xspace}
\newcommand{\spdsK}{\emph{SPDS}\xspace}
\newcommand{\dalic}{\emph{Density-Adaptive Local Iterative Clustering}\xspace}
\newcommand{\dalicK}{\emph{DALIC}\xspace}
\newcommand{\dasp}{\emph{Depth-Adaptive Superpixels}\xspace}
\newcommand{\daspK}{\emph{DASP}\xspace}
\newcommand{\sdasp}{\emph{Depth-Adaptive Superpixel Segmentation}\xspace}
\newcommand{\sdaspK}{\emph{s-DASP}\xspace}
\newcommand{\dds}{\emph{Delta Density Sampling}\xspace}
\newcommand{\ddsK}{\emph{DDS}\xspace}
\newcommand{\tdasp}{\emph{Temporal Depth-Adaptive Superpixels}\xspace}
\newcommand{\tdaspK}{\emph{t-DASP}\xspace}
\newcommand{\sshs}{\emph{Semi-Supervised Hysteresis Segmentation}\xspace}
\newcommand{\sshsK}{\emph{SSHS}\xspace}

\newcommand{\reals}{\mathbb{R}}
\newcommand{\realspos}{\mathbb{R}_{+}}
\newcommand{\atan}{\text{atan}}
\newcommand{\rgb}{[{\,0\,\vert\,1}]^3}

\newcommand{\dwrndu}{\mathcal{U}}
\newcommand{\dwrndn}{\mathcal{N}}

\newcommand{\dwp}{\mathfrak{P}}
\newcommand{\dwpb}{\mathfrak{X}}
\newcommand{\dwph}{\mathfrak{H}}
\newcommand{\dwu}{\Omega}
\newcommand{\dwg}{\mathcal{G}}
\newcommand{\dwgi}{\mathcal{G}_I}
\newcommand{\dwgs}{\mathcal{G}_S}
\newcommand{\dwgv}{\mathcal{G}_V}
\newcommand{\dwf}{\mathcal{F}}
\newcommand{\dwi}{\mathbf{I}} %{\mathcal{I}}
\newcommand{\dwv}{\mathbf{V}} %{\mathcal{V}}
\newcommand{\dwfp}{\mathfrak{f}}
\newcommand{\dwfpc}{\dwfp_c}
\newcommand{\dwfpv}{\dwfp_v}
\newcommand{\dwfpn}{\dwfp_n}
\newcommand{\dwfs}{\mathfrak{s}}
\newcommand{\dwfsc}{\dwfs_c}
\newcommand{\dwfsv}{\dwfs_v}
\newcommand{\dwfsn}{\dwfs_n}
\newcommand{\area}{\text{Area}}
\newcommand{\vol}{\text{Vol}}
\newcommand{\dwk}{\text{K}}
\newcommand{\dwdasp}{\mathfrak{D}}

\newcommand{\wt}[1]{{#1}^{(k)}}
\newcommand{\wtp}[1]{{#1}^{(k-1)}}
\newcommand{\wtz}[1]{{#1}^{(0)}}
\newcommand{\wta}[2]{{#1}^{(#2)}}

\newcommand{\dwbigo}{O}


% \nomenclature{$symbol$}{Description}

% GENERAL

\nomenclature[a]{$\mathbb{N}$}{Natural numbers (integers)}
\nomenclature[a]{$\mathbb{Z}$}{Positive integers (including 0)}
\nomenclature[a]{$\reals$}{Real numbers}
\nomenclature[a]{$\realspos$}{Positive real numbers (including 0)}
\nomenclature[a]{$\dwrndu$}{Random sample from a uniform distribution}
\nomenclature[a]{$\dwrndn$}{Random sample from a Gaussian normal distribution}

\newcommand{\seg}[1]{\text{SE}(#1)}
\nomenclature[a]{$\seg{n}$}{Special Euclidean Group which describes orientation preserving isometries of an n-dimensional rigid bodies}

\newcommand{\sog}[1]{\text{SO}(#1)}
\nomenclature[a]{$\sog{n}$}{Special Orthogonal Group which describes orientation preserving symmetries of an n-dimensional rigid bodies}

\newcommand{\sphere}[1]{\text{S}_0^{#1}}
\nomenclature[a]{$\sphere{n}$}{unit n-sphere defined as $\{x \in \reals^{n+1} \,\vert\, \|x\| = 1\}$}

\newcommand{\dwprop}{\mathcal{P}}
\nomenclature[a]{$\dwprop(X)$}{Probability of X}

\newcommand{\dwlogfnc}{L}
\nomenclature[a]{$\dwlogfnc$}{Logistic function $\dwlogfnc_\alpha(x) := \frac{1}{1+e^{-\alpha\,x}}$}

% PARTITIONS / ASP

\nomenclature[b]{$\dwp$}{Partition of a set}
\nomenclature[b]{$S$}{Segment of a partition}
\nomenclature[b]{$\dwpb$}{Ground truth / reference partition of a set}
\nomenclature[b]{$\dwph$}{Hierarchical tree of partitions}
\nomenclature[b]{$\partial S$}{Boundary of a segment}
\nomenclature[b]{$\partial \dwp$}{Boundary of a partition}
\nomenclature[b]{$\dwu$}{A domain in general}
\nomenclature[b]{$\dwgi$}{Two-dimensional finite lattice graph (for images or video frames)}
\nomenclature[b]{$\dwgv$}{Three-dimensional finite lattice graph (for videos)}
\nomenclature[b]{$\dwg$}{General graph}
\nomenclature[b]{$V$}{Set of nodes of a graph}
\nomenclature[b]{$E$}{Set of edges of a graph}
\nomenclature[b]{$\dwf$}{Image feature space}
\nomenclature[b]{$\|\cdot\|_\dwf$}{Feature space metric}
\nomenclature[b]{$\dwi$}{Mapping $\dwgi \rightarrow \dwf$}
\nomenclature[b]{$\dwv$}{Mapping $\dwgv \rightarrow \dwf$}

% DASP

\nomenclature[b]{$\dwdasp$}{Set of superpixels}
% \nomenclature[b]{$\dws$}{Superpixel}
\nomenclature[b]{$\dwgs$}{Superpixel graph}
\nomenclature[b]{$\dwfp$}{Feature of a pixel / element}
\nomenclature[b]{$\dwfs$}{Mean feature for a superpixel or segment}
\nomenclature[b]{$\area$}{Area of a 2D segment}
\nomenclature[b]{$\vol$}{Volume of a 3D segment}
\nomenclature[b]{$\rho$}{Density function}
\nomenclature[b]{$A_\rho(\,\cdot\,,U)$}{Kernel density approximation of a set of points U}
\nomenclature[b]{$E_\rho(U)$}{Error of kernel density approximation of a set of points U}

% S-DASP

\newcommand{\dwWs}{W_\dwfs}
\nomenclature[s]{$\dwWs$}{Edge weights of superpixel graph}


% T-DASP

\newcommand{\dwt}{\mathbf{T}}
\nomenclature[t]{$\dwt$}{Spatio-temporal superpixel strand}

\newcommand{\dwgt}{\mathcal{G}_\dwt}
\nomenclature[t]{$\dwgt$}{Graph of spatio-temporal superpixel strands}

\newcommand{\dwWt}{W_\dwt}
\nomenclature[s]{$\dwWt$}{Edge weights of superpixel strand graph}


% EBV

\newcommand{\dvs}{\emph{Dynamic Vision Sensor}\xspace}
\newcommand{\dvsK}{\emph{DVS}\xspace}

\newcommand{\edvs}{\emph{Embedded Dynamic Vision Sensor}\xspace}
\newcommand{\edvsK}{\emph{eDVS}\xspace}

\newcommand{\ebpf}{\emph{Event-based Particle Filter}\xspace}
\newcommand{\ebpfK}{\emph{EB-PF}\xspace}

% \newcommand{\dwedom}{\mathcal{R}}
% \nomenclature[u]{$\dwedom$}{Space for events from a dynamic vision sensor.}

\newcommand{\dwsdom}{\Omega}
\nomenclature[u]{$\dwsdom$}{Base domain for the system state used in \ebpfK.}

\newcommand{\dwr}{\mathcal{R}}
\nomenclature[u]{$\dwr$}{Pixel coordinate space for event-based sensors, i.e. $\dwr = [0,127]^2$}

\newcommand{\dwmomo}{\text{MM}}
\nomenclature[u]{$\dwmomo$}{Motion model for particle filter algorithm}

\newcommand{\dwmomob}{\text{MM}_B}
\nomenclature[u]{$\dwmomob$}{Motion model for \ebpf which processes $B$ events at once}


% EBSLAM

\newcommand{\ebslam}{\emph{Event-Based SLAM}\xspace}
\newcommand{\ebslamK}{\emph{EB-SLAM}\xspace}
\newcommand{\ebslamo}{\emph{Event-Based 2D SLAM}\xspace}
\newcommand{\ebslamoK}{\emph{EB-SLAM-2D}\xspace}
\newcommand{\ebslame}{\emph{Event-Based 3D SLAM}\xspace}
\newcommand{\ebslameK}{\emph{EB-SLAM-3D}\xspace}

\newcommand{\dwmdom}{\Gamma}
\nomenclature[v]{$\dwmdom$}{Base domain over which to build the map used in \ebslamK.}

\newcommand{\dwmm}{\mathcal{M}}
\nomenclature[v]{$\dwmm$}{A map used for mapping the environment in a SLAM method.}

\newcommand{\dwmo}{\mathcal{O}}
\nomenclature[v]{$\dwmo$}{Occurrence map used in \ebslamK}

\newcommand{\dwmz}{\mathcal{Z}}
\nomenclature[v]{$\dwmz$}{Normalization map used in \ebslamK}

\newcommand{\dwme}{\mathcal{E}}
\nomenclature[v]{$\dwme$}{Exploration map used in autonomous exploration with \ebslamK}

\newcommand{\dwgain}{\mathcal{G}}
\nomenclature[v]{$\dwgain$}{Gain map used in autonomous exploration with \ebslamK}


% DEDVS

\newcommand{\dedvsK}{\emph{D-eDVS}\xspace}
\nomenclature[w]{$\dedvsK$}{Combination of \edvsK and PrimeSense depth sensor}

\newcommand{\dweddom}{\dwr_D}
\nomenclature[w]{$\dweddom$}{Space for events from a \dedvsK sensor}


% NAMES

\nomenclature[z]{\emph{RGB}}{Refers to an RGB colour space}
\nomenclature[z]{\emph{RGB-D}}{Refers to an RGB colour space in combination with depth information}
\nomenclature[z]{\emph{ASP}}{(Density-)Adaptive Superpixel}
\nomenclature[z]{\emph{PDS}}{Poisson Disc Sampling}
\nomenclature[z]{\emph{SPDS}}{Simplified Poisson Disc Sampling}
\nomenclature[z]{\emph{DALIC}}{Density-Adaptive Local Iterative Clustering}
\nomenclature[z]{\emph{DASP}}{Depth-Adaptive Superpixels}
\nomenclature[z]{\emph{S-DASP}}{Depth-Adaptive Superpixel Segmentation}
\nomenclature[z]{\emph{UCG}}{Ultrametric contour graph}
\nomenclature[z]{\emph{UCM}}{Ultrametric contour map}
\nomenclature[z]{\emph{T-DASP}}{Temporal Depth-Adaptive Superpixel}
\nomenclature[z]{\emph{DDS}}{Delta Density Sampling}
\nomenclature[z]{\emph{TS-ASP}}{Temporal-Stable (Density-)Adaptive Superpixel}
