%!TEX root = phd.tex


\chapter{Introduction}
\label{sec:intro}


Automation plays a crucial role in current research and development projects:
humanoids shall aid in the care for elderly people, autonomous robots are developed to scout disaster areas and autonomously driving cars have the potential to increase safety and make driving more enjoyable.
For all these tasks the ability to analyse the operation environment for people, obstacles, or objects and places of interest is a fundamental requirement.
Additionally autonomous machines must be able to navigate towards theses points of interest without getting stuck or lost, and without hurting humans or hitting obstacles.
This very basic set of skills has been mastered by humans and even the smallest animals, but still poses a lot of questions and open problems for machines.

The scientific field of computer vision tackles theses problems and has made substantial progress towards solutions in the last decades.
Image segmentation has been an active field of study with extraordinary results and many approaches have been developed to detect objects in images taken by colour cameras. 
While these methods often give very good results, studying pure colour images alone is limiting, as it is often difficult to extract geometric information from projections of 3D objects.
The introduction of combined colour and depth (RGB-D) sensors has been a major step towards the analysis of complex three dimensional scenarios.

Towards autonomous navigation and exploration, numerous methods have been proposed for simultaneous localization and mapping (SLAM) or for analysing and understanding complex three-dimensional environments.
While it is a difficult computational problem to navigate in 3D without any depth information, the problem can be simplified by using a distance sensors like a laser range finder.
Robust mathematical models have been developed to fuse information into an environment map and to allow localization within the created map.

In practical applications not only the potential performance of a method in solving the task at hand is relevant, but also its resource efficiency. 
Autonomous robots are limited with respect to size, weight and electrical power consumption and thus computational resources, like CPU and RAM, have to be used efficiently.
This problems becomes increasingly important for flying exploration vehicles or embedded applications.

An approach which has the potential to provide not only functional but also efficient solutions are sparse data models.
Sparse models have been used to great success for example in machine learning \cite{park2007l1,zhu20041,chan2007direct} or for dimensionality reduction in general \cite{zou2006sparse,li2004sparse}.
In contrast to dense models which use and represent all available information, sparse models focus on the salient or relevant information.
This principle allows to concentrate attention to important aspects instead of trying to understand every piece of information.
Sometimes a sparse approach can also yield additional insight by identifying and extracting hidden patterns in complex data.  

The leading principle of this thesis is ''\emph{Efficiency through sparsity}''.
Efficiency can be viewed from two sides: an algorithm gets more efficient if it runs faster while giving the same quality of results, or if it gives better results within the same runtime.
''Sparsity'' can be realized in very different forms and here it will be mainly used to reduce the dimensionality of the visual sensor input with only minimal loss of information.
This work demonstrates that the intelligent choice of sparse models actually results in algorithms which are both faster, and at the same time give a higher quality.

\emph{Efficiency through sparsity} will be demonstrated within two main topics: superpixels and event-based vision.
The first part concentrates on the segmentation of RGB-D images and video streams by using a sparse image representation called superpixels.
Here similar pixels are grouped into small segments, called ''superpixel'', which results in a much more compact image representation which can greatly simplify the analysis of images and video streams with subsequent high-level algorithms.
In the second part a different approach to sparse models is chosen by using an event-based dynamic vision sensor which provides a sparse representation directly in hardware.
Such sensors do not provide a series of dense images, but a continuous stream of only pixel locations, where pixels are only reported when a change in illumination in this particular pixel has been registered.
Novel computer vision algorithms have to be developed for this kind of sensor which work directly on this sparse data stream and benefit from the sparse representation of dynamic changes.



\section{Depth-Adaptive Superpixels}


Classically an image is represented by a rectangular array where each entry, called ''pixel'', represents one measurement point.
Images are captures by CMOS chips which measure the intensity of red, blue and green light for each pixel and periodically map a complete measurement of all pixels into computer memory for further processing.
Today the trend goes towards increasingly bigger image resolutions with high-definition resolutions of 1080 $\times$ 1920 pixels being already at the lower end of the resolution spectrum.

But often a higher image resolution is not required for successfully understanding the contents displayed in the image.
On the contrary, a high number of pixels overloads many sophisticated methods which, as a result, often use down-sampled images with a very low resolution of e.g. only 120 to 160 pixels.
But the blind down-sampling of an image is often not a good choice as important information like rugged segment edges may be lost, while other redundant regions of an image are not compressed enough.
A very successful method to intelligently compress the information of an image are image oversegmentation techniques \cite{achanta2010,levinshtein2009,perbet2011homogeneous,ren2003,schick2012compact,shi2000ncut,weikersdorfer2012dasp,zeng2011structure} which have entered the focus of study in the recent years.

An image oversegmentation clusters pixels into segments, so called ''superpixel'', where each superpixel is a good representation of all the pixels it contains (see \reffig{intro_dasp}).
Oversegmentations do not try to immediatelly fully understand an image, instead they form a sparse, intermediate layer of information.
This sparse representation preserves segment boundaries and most of the relevant image information, and can be used by high-level algorithms for example to complete the image segmentation process or for various other applications like object tracking or saliency detection.

\begin{figure}[!t]
	\centering
	\fwn{1}
	\includegraphics{intro/sp.pdf}
	\caption[From superpixels to superpixel segmentation]{
		{\bf From left to right:} Dense pixel grid, oversegmentation (using false colours) and boundary-preserving superpixels symbolized with circles at segment barycentre with segment mean colour.
	}
	\label{fig:intro_dasp}
\end{figure}

In the recent years combined colour and depth sensors have been a great success as they simplify many applications in computer vision.
However up to now oversegmentation algorithms have focused on pure colour images.
In this work a novel oversegmentation technique, \dasp, is presented which extends the principle of superpixels to combined color and depth images.
The method forms the basis of two high-level algorithms, \sdasp and \tdasp, which compute full RGB-D image and RGB-D video segmentations.

% The methods transports the concept of image superpixels to the three-dimensional space and effectively computes oversegmentations for single-view point clouds.
% Further two high-level methods are presented which use theses superpixels to compute full image segmentations.
% \sdasp is a two-layer image segmentation technique for RGB-D images which uses spectral graph theory to segment the superpixel graph.
% \tdasp is a method to analyse RGB-D video streams which computes spatio-temporal strands of superpixels and a corresponding strand graph to compute segmentations of the spatio-temporal pixel grid of a video stream.


\section{Event-Based SLAM} 

Many results in computer vision focus on frame-based video cameras which are essentially just very fast photo cameras producing still images every few milliseconds.
Typical framerates for normal consumer products are 24 to 60 frames per seconds at high-definition resolutions.
However many motions in every day like a car moving on the road, a waving hand or a falling object require higher framerates for reliable object tracking.
While professional high speed cameras can provide several thousand frames per seconds, high frame rates imply expensive hardware, low resolution and an increased noise level.
Additionally, all frames need to be processed by the computer -- for example the PrimeSense device has a bandwidth of 26 MB/s for the colour image alone (see \reftab{ebv_bandwidth} for details).
A high bandwidth requires high computation power, high electrical power and is infeasible for many embedded realtime applications.

Dynamic vision sensors chose a different approach to encode the dynamic changes in a scene and only reports data for pixels which actually change their brightness values.
Such an approach provides a completely new view on tracking and self-localization.
Instead of working with full dense images, only a sparse stream of pixel locations with changed data needs to be processed.
However this new field of event-based computer vision requires a revisiting of established principles and the development of new ideas to adapt to the nature of dynamic vision sensors.
Dynamic vision sensors have already been used successfully in several applications and are actively investigated in current research.

Of particular interest in many computer vision applications are object tracking, self-localization and mapping, but up to know no general tracking or mapping algorithm for dynamic vision sensor has been proposed.
In this work a novel particle filter algorithm, \ebpf, is presented which the framework of Condensation to dynamic vision sensors.
The algorithm is highly efficient and thus suitable for realtime applications and embedded applications.
\ebpf is extended to a simultaneous localization and mapping algorithm -- the \ebslam algorithm -- by using a novel approach to generate environment maps.
Both algorithms demonstrate how the efficient usage of a sparse representation of dynamic changes can result in highly efficient tracking algorithms, compared to state-of-the-art which already often requires GPGPU hardware to barely run in realtime.

\begin{figure}[!t]
	\centering
	\fwn{3}
	% \includegraphics[width=0.32\columnwidth]{intro/ebv/edvs_pf.pdf}
	\includegraphics{intro/ebv/edvs_slam.pdf}
	\includegraphics{intro/ebv/edvs_exploration.pdf}
	\includegraphics{intro/ebv/eb3dslam.png}
	\caption[Applications for Event-based Vision]{
		{\bf Left:} Event-based Simultaneous Localization and Mapping;
		{\bf Middle:} Autonomous Exploration with event-based SLAM;
		{\bf Right:} Event-based 3D SLAM.
	}
	\label{fig:intro_eb}
\end{figure}



\section{Publications}

Parts of the material discussed in this thesis was published and presented at peer-reviewed computer vision and robotics conferences:

% \subsubsection{Conferences}

\begin{itemize}

\item
David Weikersdorfer, David Gossow and Michael Beetz:
\emph{Depth-Adaptive Superpixels}.
Proceedings of the International Conference on Pattern Recognition (ICPR), Tokyo, Japan, 2012

\item
David Weikersdorfer and J\"org Conradt:
\emph{Event-Based Particle Filtering for Robot Self-Localization}.
Proceedings of the International Conference on Robotics and Biomimetics (ROBIO), Guangzhou, China, 2012

\item
David Weikersdorfer, Alexander Schick and Daniel Cremers:
\emph{Depth-Adaptive Supervoxel for Efficient RGB-D Video Analysis}.
Proceedings of the International Conference on Image Processing (ICIP), Melbourne, Australia, 2013

\item
David Weikersdorfer, Raoul Hoffmann and J\"org Conradt:
\emph{Simultaneous Localization and Mapping for event-based Vision Systems}.
Proceedings of the International Conference on Computer Vision Systems (ICVS), St. Petersburg, Russia, 2013

\item
Raoul Hoffmann, David Weikersdorfer and J\"org Conradt:
\emph{Autonomous Indoor Exploration with an Event-Based Visual SLAM System}.
Proceedings of the European Conference on Mobile Robots (ECMR), Barcelona, Spain, 2013

\item
David Weikersdorfer, David Adrian, Daniel Cremers and J\"org Conradt:
\emph{Event\-based 3D SLAM with a depth-augmented dynamic vision sensor}.
{\bf SUBMITTED} at International Conference on Robotics and Automation (ICRA), Hong Kong, 2014

\end{itemize}

% \subsubsection{Journals}

% \begin{itemize}

% \item
% David Weikersdorfer and J\"org Conradt:
% \emph{Event-based vision: Tracking and modeling for dynamic vision sensors}.
% {\bf PREPARED} for Frontiers in Neuroscience, 2014

% \end{itemize}

% \clearpage


\section{Outline and Contributions}

This thesis is structured as follows (see \reffig{intro_outline}): This introduction, Depth-Adaptive Superpixels (part I), Event-based SLAM (part II), and Conclusions and appendix.
Part I and II are self-contained and can be studied in isoloation.

At the beginning of the first part in \refsec{asp} the \asp (\aspK) oversegmentation algorithm is introduced which extends the state-of-the-art algorithm SLIC to non-constant density functions.
In order to run in realtime a fast Poisson disc sampling algorithm is developed (\refsec{asp_poisson}) and a density-dependent compactness term is introduced for a local iterative cluster algorithm (\refsec{asp_dalic}).
\aspK forms the basis of the \dasp (\daspK) oversegmentation algorithm for RGB-D images (\refsec{dasp}).
\daspK transports the concept of uniform and compact superpixels from 2D to 3D and can also be viewed as an oversegmentation algorithm for single-view point clouds.
For the full segmentation of RGB-D images a new graph based method is developed which results in the \sdasp (\sdaspK) algorithm (\refsec{sdasp}).
It is demonstrated how a local similarity measure on a graph of superpixels can be globalized with the help of spectral graph theory to yield good image segments.
\daspK is additionally extended to the RGB-D video stream segmentation method \tdasp (\tdaspK) which uses strands of superpixels and a graph structure on theses strands to provide very good results for RGB-D video stream analysis (\refsec{tdasp}).

In the second part, the \ebpf (\ebpfK) algorithm for dynamic vision sensors is introduced (\refsec{ebv}).
This algorithm is a novel approach to object tracking and self-localization using only a sparse stream of pixel events.
\ebpfK is extended to a novel simultaneous localization and mapping algorithm -- the \ebslam (\ebslamK) algorithm -- by using an intelligent and surprisingly simple method to represent and generate environment maps (\refsec{ebslam}).
The algorithm is applied in an autonomous exploration scenario where a small robot in the style of a vacuum cleaner creates a map of an unknown environment (\refsec{ebslam_app_explore}).
In \refsec{eb3} the \ebslamK algorithm is adapted and reformulated for the three-dimensional case leading to an highly efficient 3D SLAM algorithm (\ebslameK).

All proposed methods are further introduced and motivated in their respective chapters.
At the end of each chapter a thorough evaluation of the proposed methods is presented.
All evaluations include a comparison to ground truth data and if applicable a comparision to comparable state-of-the-art methods.

The thesis is concluded in \refsec{conclusions} with a revision of the presented methods and remarks on possible future work.
In the appendix a listing of quality metrics for superpixels (\refsec{apx_metric}) and supplementary evaluation results (\refsec{apx_results}) can be found.

\begin{figure}
	\centering
	\fwn{1}
	\includegraphics{intro/outline/outline.pdf}
	\caption[Thesis outline]{Outline of this thesis}
	\label{fig:intro_outline}
\end{figure}

